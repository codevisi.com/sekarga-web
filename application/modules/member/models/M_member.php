<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_member extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function index() {
		$data['view'] = 'v_member';

		return $data;
	}
	
	function register($post) {
		$data['view'] = 'v_register';
		$data['nama'] = $post['nama'];
		$data['nopeg'] = $post['nopeg'];
		$data['email'] = $post['email'];

		return $data;
	}

	function signup($post) {
		// insert data
		regNewUser($post);

		//sendmail
		$mailReg = sendmail($post['email'], $post['nama'], 'Registration', $post);
		if ($mailReg) $data = TRUE;
		else $data = FALSE;		

		return $data;
	}

	function home() {
		$data['view'] = 'v_member_home';
		$data['profile'] = getDetailUser($this->session->userdata('username'));
		$data['photo'] = getPhoto($this->session->userdata('username'));

		return $data;
	}

	function lostpassword() {
		$data['view'] = 'v_member_lostpass';
		$data['profile'] = getDetailUser($this->session->userdata('username'));

		return $data;
	}

	function changepass() {
		$data['view'] = 'v_member_changepass';
		$data['profile'] = getDetailUser($this->session->userdata('username'));
		$data['photo'] = getPhoto($this->session->userdata('username'));

		return $data;
	}

	function idsekarga() {
		$data['view'] = 'v_member_idsekarga';
		$data['profile'] = getDetailUser($this->session->userdata('username'));
		$data['photo'] = getPhoto($this->session->userdata('username'));

		return $data;
	}

	function lapkeu() {
		$data['view'] = 'v_member_lapkeu';
		$data['lapkeu'] = getLapKeu();
		$data['profile'] = getDetailUser($this->session->userdata('username'));
		$data['photo'] = getPhoto($this->session->userdata('username'));

		return $data;
	}
    
}