<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MX_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('M_member');
    }

    function index() {
        $isLogin = isLogin();
        if ($isLogin) {
            $isMember = isMember($this->session->userdata('username'));
            if ($isMember) {
                redirect('member/home', 'refresh');
            } else {
                $data = $this->M_member->index();
                $this->load->view('main/v_main', $data);    
            }
        } else {        
            $data = $this->M_member->index();
            $this->load->view('main/v_main', $data);
        }
    }
    
    function register() {
        if ($this->input->post()) {
            $isMember = isMember($this->input->post('email'), $this->input->post('nopeg'));
            if ($isMember) {
                if ($isMember == 2)
                    $this->session->set_flashdata('msg', 'Registrasi anda dalam proses verifikasi data');                    
                else if ($isMember == 1) 
                    $this->session->set_flashdata('msg', 'Nopeg dan Email anda sudah terdaftar sebagai anggota SEKARGA');                    
                
                $this->index();
            } else {
                $data = $this->M_member->register($this->input->post());
                $this->load->view('main/v_main', $data);
            }
        } else {
            $this->index();
        }
    }

    function signup() {
        if ($this->input->post()) {
            $data = $this->M_member->signup($this->input->post());
            if ($data) $this->session->set_flashdata('emailsend', 'Email konfirmasi registrasi berhasil dikirim ke email anda');
            else $this->session->set_flashdata('emailsend', 'Email konfirmasi registrasi gagal dikirim ke email anda, cek alamat email anda dan hubungi kami untuk melakukan perubahan');
            redirect('member/signup_success', 'refresh');
        } else {
            $this->register();
        }
    }

    function signup_success() {
        $data['view'] = 'v_tq';
        $this->load->view('main/v_main', $data);
    }

    function signin() {
        if ($this->input->post()) {
            $user = trim($this->input->post('username'));
            $pass = trim($this->input->post('userpass'));
            $auth = authenticate($user, $pass);
            if ($auth) {
                setSession($auth);
                redirect('member/home', 'refresh');
            } else {
                $isMember = isMember($user);
                if ($isMember == 2)
                    $this->session->set_flashdata('signin', 'Registrasi anda dalam proses verifikasi data');                    
                else if ($isMember == 1) 
                    $this->session->set_flashdata('signin', 'Password yang anda masukkan salah');                    
                else
                   $this->session->set_flashdata('signin', 'Anda belum terdaftar sebagai anggota Sekarga. Silahkan mendaftar terlebih dahulu.');

                $this->index();
            }
        } else 
            $this->index();
    }

    function lostpassword() {
        if ($this->input->post()) {
            $username = trim($this->input->post('username'));
            $passgen = passgen();
            changePass($username, $passgen);
            $detailUser = getDetailUser($username);      
            if ($detailUser) {
                $messageAdd = "<hr><p>Password baru anda : ".$passgen."</p><p>Silahkan Login ke halaman member Sekarga  <a href='https://www.sekarga.or.id/member'>https://www.sekarga.or.id/member</a></p>";  

                $mailReg = sendmail($username, $detailUser[0]['nama'], 'Lost', $messageAdd);
                if ($mailReg) $this->session->set_flashdata('msg', 'Password berhasil dikirim ke email anda');
                else $this->session->set_flashdata('msg', 'Password gagal dikirim ke email anda');
            } else
                $this->session->set_flashdata('msg', 'Email anda tidak terdaftar');

            $data = $this->M_member->lostpassword();
            $this->load->view('main/v_main', $data);  
        } else {
            $data = $this->M_member->lostpassword();
            $this->load->view('main/v_main', $data);  
        }        
    }

    function home() {
        $isLogin = isLogin();
        $isMember = isMember($this->session->userdata('username'));
        if ($isLogin && $isMember) {
            if ($this->input->post()) {
                $data = array(
                    'nama' => strtoupper($this->input->post('nama')),
                    'unit' => strtoupper($this->input->post('unit')),
                    'position' => $this->input->post('position'),
                    'phone' => $this->input->post('phone'),
                    'address' => $this->input->post('address')                    
                );

                $where = array(
                    'username' => $this->session->userdata('username')
                );

                update_data($where, 'tbl_user', $data);
                $this->session->set_flashdata('msg', 'Perubahan data profil anda berhasil disimpan...'); 
                
                $data = $this->M_member->home();
                $this->load->view('main/v_main', $data);    
            } else {
                $data = $this->M_member->home();
                $this->load->view('main/v_main', $data);
            }
        } else 
            redirect('member','refresh');
            
    }

    function changepass() {
        $isLogin = isLogin();
        $isMember = isMember($this->session->userdata('username'));
        if ($isLogin && $isMember) {            
            if ($this->input->post()) {
                $isOldPass = checkOldPass($this->session->userdata('username'), $this->input->post('oldpass'));
                if ($isOldPass) {
                    changePass($this->session->userdata('username'), $this->input->post('newpass1'));
                    $this->session->set_flashdata('msg', 'Password anda berhasil diganti ...'); 
                    $data = $this->M_member->changepass();
                    $this->load->view('main/v_main', $data);
                } else {
                    $this->session->set_flashdata('msg', 'Password lama anda salah ...'); 
                    $data = $this->M_member->changepass();
                    $this->load->view('main/v_main', $data);
                }
            } else {
                $data = $this->M_member->changepass();
                $this->load->view('main/v_main', $data);
            }            
        } else 
            redirect('member','refresh');
        
    }

    function logout() {
        $this->session->sess_destroy();
        header('location:'.base_url());
    }

    function upload_profil_photo() {
        $this->load->library('upload');
        $config['upload_path'] = './assets/img/member/'; 
        $config['allowed_types'] = 'jpg|png|jpeg'; 
        $config['encrypt_name'] = TRUE; 

        $this->upload->initialize($config);
        if(!empty($_FILES['file']['name'])){
            if ($this->upload->do_upload('file')){
                //delete if exist
                deletePhoto($this->session->userdata('username'));
                $gbr = $this->upload->data();                
                $this->resize_image($gbr);
                //Compress Image
                /* $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 600;
                $config['height']= 400;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
 
                $gambar=$gbr['file_name'];
                $judul=$this->input->post('xjudul');
                $this->m_upload->simpan_upload($judul,$gambar);
                echo "Image berhasil diupload"; */
            }                      
        }else{
            echo "Image yang diupload kosong";
        }
    }

    function resize_image($image_data){
        $this->load->library('image_lib');
        $w = $image_data['image_width'];
        $h = $image_data['image_height'];
    
        $n_w = 400;
        $n_h = 400;
    
        $source_ratio = $w / $h;
        $new_ratio = $n_w / $n_h;
        if($source_ratio != $new_ratio){
    
            $config['image_library'] = 'gd2';
            // $config['source_image'] = './uploads/uploaded_image.jpg';
            $config['source_image']='./assets/img/member/'.$image_data['file_name'];
            $config['maintain_ratio'] = FALSE;
            if($new_ratio > $source_ratio || (($new_ratio == 1) && ($source_ratio < 1))){
                $config['width'] = $w;
                $config['height'] = round($w/$new_ratio);
                $config['y_axis'] = round(($h - $config['height'])/2);
                $config['x_axis'] = 0;
    
            } else {
    
                $config['width'] = round($h * $new_ratio);
                $config['height'] = $h;
                $size_config['x_axis'] = round(($w - $config['width'])/2);
                $size_config['y_axis'] = 0;
    
            }
    
            $this->image_lib->initialize($config);
            $this->image_lib->crop();
            $this->image_lib->clear();
        }

        $config['image_library'] = 'gd2';
        $config['source_image']='./assets/img/member/'.$image_data['file_name'];
        $config['new_image'] = './assets/img/member/'.$image_data['file_name'];
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $n_w;
        $config['height'] = $n_h;
        $this->image_lib->initialize($config);
    
        if (!$this->image_lib->resize()){
            echo $this->image_lib->display_errors();
        } else {
            //update profile
            $data = array(
                'photo' => $image_data['file_name']
            );

            $where = array(
                'username' => $this->session->userdata('username')
            );

            update_data($where, 'tbl_user', $data);
            echo "Photo profil berhasil diupload";
        }
    }
    
    function idsekarga() {
        $data = $this->M_member->idsekarga();
        $this->load->view('main/v_main', $data);
    }

    function lapkeu() {
        $data = $this->M_member->lapkeu();
        $this->load->view('main/v_main', $data);
    }    
    
}