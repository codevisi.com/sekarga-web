<div role="main" class="main">

    <section class="page-header page-header-classic page-header-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                    <h1 data-title-border>MEMBER AREA</h1>
                </div>
                <div class="col-md-4 order-1 order-md-2 align-self-center">
                    <ul class="breadcrumb d-block text-md-right">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="active">Member</li>
                        <li class="active">Kartu Anggota Digital</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-2">

        <div class="row">
            <div class="col-lg-3 mt-4 mt-lg-0">

                <div class="d-flex justify-content-center mb-4">
                    <div class="profile-image-outer-container">
                        <div class="profile-image-inner-container bg-color-primary">
                            <?php if ($photo) { ?>
                                <img src="<?php echo base_url();?>assets/img/member/<?php echo $photo;?>">
                            <?php } else { ?>
                                <img src="<?php echo base_url();?>assets/img/avatars/avatar.jpg">
                            <?php } ?>                            
                            <span class="profile-image-button bg-color-dark">
                                <i class="fas fa-camera text-light"></i>
                            </span>
                        </div>
                        <input type="file" id="file" name="filephoto" class="profile-image-input">
                    </div>
                </div>


                <aside class="sidebar mt-2" id="sidebar">
                    <ul class="nav nav-list flex-column mb-5">
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/home">Profil Keanggotaan</a></li>
                        <li class="nav-item"><a class="nav-link text-dark active" href="<?php echo base_url();?>member/idsekarga">Kartu Anggota Digital</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/lapkeu">Laporan Keuangan</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/changepass">Ganti Password</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/logout">Logout</a></li>
                    </ul>
                </aside>

            </div>
            <div class="col-lg-6">
                <div class="overflow-hidden mb-1">
                    <h2 class="font-weight-normal text-center text-7 mb-0"><strong class="font-weight-extra-bold">Kartu Anggota</strong>
                        Digital</h2>
                </div>              
                <hr><br>  
                <section class="section section-height-6 bg-light border-bottom-0 border-top-0 m-0" style="background-image: url(<?php echo base_url();?>assets/img/member/sekargaid.jpg); background-repeat: no-repeat; background-position: center top;">                   
                    <div class="d-flex justify-content-center" style="margin-top:-100px">
                        <div class="profile-image-inner-container">
                            <?php if ($photo) { ?>
                                <img src="<?php echo base_url();?>assets/img/member/<?php echo $photo;?>" width="200">
                            <?php } else { ?>
                                <img src="<?php echo base_url();?>assets/img/avatars/avatar.jpg" width="200">
                            <?php } ?>                                                           
                        </div>
                    </div>         
                    <div class="d-flex justify-content-center mt-4 text-5 font-weight-bold">
                        <?php echo $this->session->userdata('nama');?>
                    </div>           
                    <div class="d-flex justify-content-center mt-0 text-5 font-weight-bold">
                        <?php echo $this->session->userdata('nopeg');?>
                    </div>           
                </section>
            </div>
            <div class="col-lg-3"></div>            
        </div>

    </div>

</div>