<div role="main" class="main">
    <section class="page-header page-header-classic page-header-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                    <h1 data-title-border>GABUNG BERSAMA KAMI</h1>
                </div>
                <div class="col-md-4 order-1 order-md-2 align-self-center">
                    <ul class="breadcrumb d-block text-md-right">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="active">Masuk/Daftar</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col">

                <div class="featured-boxes">
                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="featured-box featured-box-primary text-left mt-5">                                
                                <div class="box-content">
                                    <?php if ($this->session->flashdata('msg')) { ?>
                                    <div class="alert alert-info alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <i class="fas fa-exclamation-triangle"></i><?php echo $this->session->flashdata('msg');?>                                        
                                    </div>
                                    <?php } ?>
                                    <h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-3">Daftar Anggota Sekarga</h4> 
                                    <form action="<?php echo base_url();?>member/register" id="frmSignUp" method="post">
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <label class="font-weight-bold text-dark text-2">Nama</label>
                                                <input type="text" name="nama" value="" class="form-control form-control-lg"
                                                    required>
                                            </div>
                                        </div>   
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <label class="font-weight-bold text-dark text-2">Nopeg</label>
                                                <input type="number" name="nopeg" value="" class="form-control form-control-lg"
                                                    required>
                                            </div>
                                        </div> 
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <label class="font-weight-bold text-dark text-2">E-mail</label>
                                                <input type="email" name="email" value="" class="form-control form-control-lg"
                                                    required>
                                            </div>
                                        </div>                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-9">
                                                <label class="text-2">Klik register untuk menuju form pendaftaran</label>
                                            </div>
                                            <div class="form-group col-lg-3 text-4">
                                                <input type="submit" value="Register"
                                                    class="btn btn-primary btn-modern float-right"
                                                    data-loading-text="Loading...">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="featured-box featured-box-primary text-left mt-5">
                                <div class="box-content">
                                    <?php if ($this->session->flashdata('signin')) { ?>
                                    <div class="alert alert-info alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <i class="fas fa-exclamation-triangle"></i><?php echo $this->session->flashdata('signin');?>                                        
                                    </div>
                                    <?php } ?>
                                    <h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-3">Login Anggota</h4>
                                    <form action="<?php echo base_url();?>member/signin" id="formRegister" method="post">
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <label class="font-weight-bold text-dark text-2">Email
                                                    </label>
                                                <input type="email" name="username" value="" class="form-control form-control-lg"
                                                    required>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col">                                                
                                                <label class="font-weight-bold text-dark text-2">Password</label>
                                                <input name="userpass" type="password" value="" class="form-control form-control-lg"
                                                    required>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="rememberme">
                                                    <label class="custom-control-label text-2" for="rememberme">Remember
                                                        Me</label>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <a class="float-right" href="<?php echo base_url();?>member/lostpassword">(Lupa Password?)</a>
                                            </div>
                                        </div>  
                                        <br>
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                                <div class="g-recaptcha" data-sitekey="6LcoFqgZAAAAAA9G-mf4g9Dyh6omNzqYtbQUsQor"></div>
                                            </div>
                                            <div class="form-group col-lg-6 text-4">
                                                <input type="submit" value="Login"
                                                    class="btn btn-primary btn-modern float-right"
                                                    data-loading-text="Loading...">
                                            </div>                                            
                                        </div>                                      
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

