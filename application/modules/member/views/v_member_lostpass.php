<div role="main" class="main">

    <section class="page-header page-header-classic page-header-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                    <h1 data-title-border>FORGOT PASSWORD</h1>
                </div>
                <div class="col-md-4 order-1 order-md-2 align-self-center">
                    <ul class="breadcrumb d-block text-md-right">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="active">Masuk/Daftar</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="container py-2">

        <div class="row">            
            <div class="col-lg-12 order-1 order-lg-2">
                
                <div class="row justify-content-md-center"">
                    <div class="col-md-9">
                        <div class="featured-box featured-box-primary text-left mt-0">
                            <div class="box-content">
                                <h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-0">Lupa Password?</h4>
                                <p class="text-2 opacity-7">Masukkan email anda dibawah ini, kami akan mengirimkan password baru ke email anda.</p>
                                <?php if ($this->session->flashdata('msg')) { ?>
                                <div class="alert alert-info alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo $this->session->flashdata('msg');?>
                                </div>
                                <?php } ?>
                                <form action="<?php echo base_url();?>member/lostpassword" id="frmLostPassword" method="post" class="needs-validation">
                                    <div class="form-row">
                                        <div class="form-group col">
                                            <label class="font-weight-bold text-dark text-2">E-mail anda yang terdaftar</label>
                                            <input type="email" name="username" value="" class="form-control form-control-lg" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col">
                                            <input type="submit" value="Send" class="btn btn-primary btn-modern float-right" data-loading-text="Loading...">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    
</div>