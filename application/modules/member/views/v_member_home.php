<div role="main" class="main">

    <section class="page-header page-header-classic page-header-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                    <h1 data-title-border>MEMBER AREA</h1>
                </div>
                <div class="col-md-4 order-1 order-md-2 align-self-center">
                    <ul class="breadcrumb d-block text-md-right">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="active">Member</li>
                        <li class="active">Profile Keanggotaan</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-2">

        <div class="row">
            <div class="col-lg-3 mt-4 mt-lg-0">

                <div class="d-flex justify-content-center mb-4">
                    <div class="profile-image-outer-container">
                        <div class="profile-image-inner-container bg-color-primary">
                            <?php if ($photo) { ?>
                                <img src="<?php echo base_url();?>assets/img/member/<?php echo $photo;?>">
                            <?php } else { ?>
                                <img src="<?php echo base_url();?>assets/img/avatars/avatar.jpg">
                            <?php } ?>                            
                            <span class="profile-image-button bg-color-dark">
                                <i class="fas fa-camera text-light"></i>
                            </span>
                        </div>
                        <input type="file" id="file" name="filephoto" class="profile-image-input">
                    </div>
                </div>

                <aside class="sidebar mt-2" id="sidebar">
                    <ul class="nav nav-list flex-column mb-5">
                        <li class="nav-item"><a class="nav-link text-dark active" href="<?php echo base_url();?>member/home">Profil Keanggotaan</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/idsekarga">Kartu Anggota Digital</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/lapkeu">Laporan Keuangan</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/changepass">Ganti Password</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/logout">Logout</a></li>
                    </ul>
                </aside>

            </div>
            <div class="col-lg-9">

                <div class="overflow-hidden mb-1">
                    <h2 class="font-weight-normal text-7 mb-0"><strong class="font-weight-extra-bold">Profil</strong>
                        Keanggotaan</h2>
                </div>
                <div class="overflow-hidden mb-4 pb-3">
                    <p class="mb-0">Detail keanggotaan Sekarga anda</p>
                </div>

                <?php if ($this->session->flashdata('msg')) { ?>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <i class="far fa-thumbs-up"></i><?php echo $this->session->flashdata('msg');?>
                </div>
                <?php } ?>

                <form action="<?php echo base_url();?>member/home" method="post" role="form" class="needs-validation">
                    <?php foreach($profile as $user) { ?>
                    <div class="form-group row">
                        <label
                            class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2 required">No. Anggota</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="noanggota" readonly type="text" value="<?php echo $user['no_anggota'];?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label
                            class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2 required">Nama</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="nama" required type="text" value="<?php echo ucwords(strtolower($user['nama']));?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label
                            class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2 required">Nopeg</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="nopeg" readonly required type="number" value="<?php echo $user['nopeg'];?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label
                            class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2 required">Unit</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="unit" required type="text" value="<?php echo $user['unit'];?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label
                            class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Jabatan</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="position" type="text" value="<?php echo $user['position'];?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label
                            class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Email *</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="email" readonly type="email" value="<?php echo $user['email'];?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label
                            class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Phone *</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="phone" type="number" value="<?php echo $user['phone'];?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label
                            class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Alamat</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="address" type="text" value="<?php echo $user['address'];?>   ">
                        </div>
                    </div>                     
                    <div class="form-group row">
                        <div class="form-group col-lg-9">

                        </div>
                        <div class="form-group col-lg-3">
                            <input type="submit" value="Simpan" class="btn btn-primary btn-modern float-right"
                                data-loading-text="Loading...">
                        </div>
                    </div>
                    <?php } ?>
                </form>

            </div>
        </div>

    </div>

</div>