<div role="main" class="main">
    <section class="page-header page-header-classic page-header-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                    <h1 data-title-border>REGISTRASI ANGGOTA BERHASIL</h1>
                </div>
                <div class="col-md-4 order-1 order-md-2 align-self-center">
                    <ul class="breadcrumb d-block text-md-right">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="active">Register</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    
    <div class="container">                 
        <div class="row">
            <div class="col text-center">
                <h2 class="font-weight-normal text-7 mb-2"><strong class="font-weight-extra-bold">Terima Kasih telah mendaftar sebagai Anggota Sekarga,</strong></h2>
                <p class="mb-0 lead">Pendaftaran anda sebagai anggota Sekarga sudah masuk ke sistem kami, kami akan melakukan verifikasi data anda terlebih dahulu. Setelah data anda ter-verifikasi, kami akan mengirim notifikasi pemberitahuan via email yang berisi detail keanggotaan anda dan username/password untuk mengakses halaman anggota pada web Sekarga. </p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <hr class="solid my-5">
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <p class="mb-1"><?php echo $this->session->flashdata('emailsend');?></p>
            </div>
        </div>
    </div>
</div>