<div role="main" class="main">

    <section class="page-header page-header-classic page-header-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                    <h1 data-title-border>FORM PENDAFTARAN</h1>
                </div>
                <div class="col-md-4 order-1 order-md-2 align-self-center">
                    <ul class="breadcrumb d-block text-md-right">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="active">Register</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    
    <div class="container">
        <div class="row">
            <div class="col">
                <section class="card card-admin">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                        </div>

                        <h2 class="card-title">Form Pendaftaran Anggota Baru Sekarga</h2>
                    </header>                    
                    <div class="card-body">
                        <form action="<?php echo base_url();?>member/signup" class="form-horizontal form-bordered" method="post" id="formRegister">
                            <div class="form-group row">
                                <label class="col-lg-3 control-label text-lg-right pt-2"
                                    for="inputDefault">Nama</label>
                                <div class="col-lg-6">
                                    <input name="nama" type="text" class="form-control" value="<?php echo $nama;?>" id="inputDefault" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 control-label text-lg-right pt-2"
                                    for="inputDefault">Nomor Pegawai</label>
                                <div class="col-lg-6">
                                    <input name="nopeg" type="number" class="form-control" value="<?php echo $nopeg;?>"  id="inputDefault" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 control-label text-lg-right pt-2"
                                    for="inputDefault">Unit</label>
                                <div class="col-lg-6">
                                    <input name="unit" type="text" class="form-control" id="inputDefault" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 control-label text-lg-right pt-2"
                                    for="inputDefault">Nomor HP</label>
                                <div class="col-lg-6">
                                    <input name="nohp" type="number" class="form-control" id="inputDefault" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 control-label text-lg-right pt-2"
                                    for="inputDefault">Email</label>
                                <div class="col-lg-6">
                                    <input name="email" type="email" class="form-control" value="<?php echo $email;?>"  id="inputDefault" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-1">
                                </div> 
                                <div class="col-lg-10">
                                    <p align="justify">Atas dasar Undang-Undang No. 21 Tahun 2000 Tentang Serikat Pekerja. KEPUTUSAN MENTERI TENAGA KERJA DAN TRANSMIGRASI REPUBLIK INDONESIA NOMOR: KEP-187/MEN/IX/2004, Tentang Iuran Anggota Serikat Pekerja, ART Sekarga dan Keputusan MUBES V Sekarga 2013, maka dengan ini saya menyatakan mendaftarkan diri untuk menjadi Anggota SEKARGA dan memberikan Kuasa kepada Pengurus SEKARGA untuk memotong Iuran Anggota saya sebesar Rp. 30.000,- (Tiga Puluh Ribu Rupiah) yang dipotong dari Gaji saya pada setiap bulan. </p>

                                    <p align="justify">Demikian Formulir Pendaftaran dan Kuasa Pemotongan Iuran ini saya buat dengan sebenar-benarnya dan tanpa ada unsur paksaan dari siapapun.</p>

                                    <p>Jakarta, <?php echo date('d F Y');?></p>
                                    <p>&nbsp;</p>
                                    <p><?php echo ucwords(strtolower($nama));?></p>
                                    <p>&nbsp;</p>
                                    <p>(*) Keputusan MUBES V Sekarga 2013 memutuskan, Iuran Anggota menjadi Rp. 30.000,- per bulan/sama nilainya untuk semua Anggota</p>
                                </div> 
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-5"><div class="g-recaptcha" data-sitekey="6LcoFqgZAAAAAA9G-mf4g9Dyh6omNzqYtbQUsQor"></div></div>
                                <div class="col-lg-3 text-6">
                                    <input type="submit" value="Setuju &amp; Kirim Registrasi"
                                        class="btn btn-primary btn-modern float-right"
                                        data-loading-text="Loading...">
                                </div>                                
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>