<div role="main" class="main">

    <section class="page-header page-header-classic page-header-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                    <h1 data-title-border>MEMBER AREA</h1>
                </div>
                <div class="col-md-4 order-1 order-md-2 align-self-center">
                    <ul class="breadcrumb d-block text-md-right">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="active">Member</li>
                        <li class="active">Laporan Keuangan</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-2">

        <div class="row">
            <div class="col-lg-3 mt-4 mt-lg-0">

                <div class="d-flex justify-content-center mb-4">
                    <div class="profile-image-outer-container">
                        <div class="profile-image-inner-container bg-color-primary">
                            <?php if ($photo) { ?>
                                <img src="<?php echo base_url();?>assets/img/member/<?php echo $photo;?>">
                            <?php } else { ?>
                                <img src="<?php echo base_url();?>assets/img/avatars/avatar.jpg">
                            <?php } ?>                            
                            <span class="profile-image-button bg-color-dark">
                                <i class="fas fa-camera text-light"></i>
                            </span>
                        </div>
                        <input type="file" id="file" name="filephoto" class="profile-image-input">
                    </div>
                </div>

                <aside class="sidebar mt-2" id="sidebar">
                    <ul class="nav nav-list flex-column mb-5">
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/home">Profil Keanggotaan</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/idsekarga">Kartu Anggota Digital</a></li>
                        <li class="nav-item"><a class="nav-link text-dark active" href="<?php echo base_url();?>member/lapkeu">Laporan Keuangan</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/changepass">Ganti Password</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/logout">Logout</a></li>
                    </ul>
                </aside>

            </div>
            <div class="col-lg-9">

                <div class="overflow-hidden mb-1">
                    <h2 class="font-weight-normal text-7 mb-0">Laporan <strong class="font-weight-extra-bold">Keuangan</strong>
                        </h2>
                </div>
                <div class="overflow-hidden mb-4 pb-3">
                    <p class="mb-0">Laporan Keuangan DPP &amp; DPC Sekarga</p>
                </div>                

                <?php if ($lapkeu): ?>
                <div class="row">                    
                    <div class="col-lg-12">
                        <div class="tabs tabs-vertical tabs-right tabs-primary">
                            <div class="tab-content">
                                <?php foreach ($lapkeu as $lap) { ?>                                
                                <div id="<?php echo str_replace(" ","",strtolower($lap['lap_name']));?>" class="tab-pane">
                                    <p><?php echo $lap['lap_name'];?></p>
                                    <p>
                                        <span class='zoom' id='ex1'>
                                        <img alt="" class="img-fluid" src="<?php echo base_url();?>assets/img/lapkeu/<?php echo $lap['doc'];?>">
                                        </span>
                                    </p>
                                </div>
                                <?php } ?>                                
                            </div>
                            <ul class="nav nav-tabs">
                                <?php foreach ($lapkeu as $lap) { ?>                                
                                <li class="nav-item active">
                                    <a class="nav-link" href="#<?php echo str_replace(" ","",strtolower($lap['lap_name']));?>" data-toggle="tab"><?php echo $lap['lap_name'];?></a>
                                </li>                                
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>