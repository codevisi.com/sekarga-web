<div role="main" class="main">

    <section class="page-header page-header-classic page-header-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                    <h1 data-title-border>MEMBER AREA</h1>
                </div>
                <div class="col-md-4 order-1 order-md-2 align-self-center">
                    <ul class="breadcrumb d-block text-md-right">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="active">Member</li>
                        <li class="active">Ganti Password</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-2">

        <div class="row">
            <div class="col-lg-3 mt-4 mt-lg-0">

                <div class="d-flex justify-content-center mb-4">
                    <div class="profile-image-outer-container">
                        <div class="profile-image-inner-container bg-color-primary">
                            <?php if ($photo) { ?>
                                <img src="<?php echo base_url();?>assets/img/member/<?php echo $photo;?>">
                            <?php } else { ?>
                                <img src="<?php echo base_url();?>assets/img/avatars/avatar.jpg">
                            <?php } ?>                            
                            <span class="profile-image-button bg-color-dark">
                                <i class="fas fa-camera text-light"></i>
                            </span>
                        </div>
                        <input type="file" id="file" name="filephoto" class="profile-image-input">
                    </div>
                </div>


                <aside class="sidebar mt-2" id="sidebar">
                    <ul class="nav nav-list flex-column mb-5">
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/home">Profil Keanggotaan</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/idsekarga">Kartu Anggota Digital</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/lapkeu">Laporan Keuangan</a></li>
                        <li class="nav-item"><a class="nav-link text-dark active" href="<?php echo base_url();?>member/changepass">Ganti Password</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>member/logout">Logout</a></li>
                    </ul>
                </aside>

            </div>
            <div class="col-lg-9">

                <div class="overflow-hidden mb-1">
                    <h2 class="font-weight-normal text-7 mb-0"><strong class="font-weight-extra-bold">Ganti</strong>
                        Password</h2>
                </div>
                <div class="overflow-hidden mb-4 pb-3">
                    <p class="mb-0">Ganti password akun anda di website Sekarga</p>
                </div>

                <?php if ($this->session->flashdata('msg')) { ?>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php echo $this->session->flashdata('msg');?>
                </div>
                <?php } ?>

                <form action="<?php echo base_url();?>member/changepass" method="post" role="form" id="loginForm">
                    <?php foreach($profile as $user) { ?>
                    <div class="form-group row">
                        <label
                            class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2 required">Password Lama</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="oldpass" type="password" value="" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label
                            class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2 required">Password Baru</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="newpass1" id="newpass1" type="password" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label
                            class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2 required">Konfirmasi Password Baru</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="newpass2" id="newpass2" type="password" required>
                        </div>
                    </div>                   
                    <div class="form-group row">
                        <div class="form-group col-lg-9">

                        </div>
                        <div class="form-group col-lg-3">
                            <input type="submit" value="Simpan" class="btn btn-primary btn-modern float-right"
                                data-loading-text="Loading...">
                        </div>
                    </div>
                    <?php } ?>
                </form>

            </div>
        </div>

    </div>

</div>