<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_news extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function index() {        
        $nItem = countNews();
        $this->load->library('pagination');
        $config['base_url'] = base_url().'news/index/';
		$config['total_rows'] = $nItem;
        $config['per_page'] = 5;
        $from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
        $data['newsList'] = getNews($config['per_page'], $from);
        $data['newsCat'] = getCat();
        $data['view'] = 'v_news';

        return $data;
    }

    function detail($id) {
        $data['newsDetail'] = getNewsDetail($id);
        $data['newsCat'] = getCat();
        $data['view'] = 'v_news_detail';

        return $data;
    }

    function cat($catid) {
        $nItem = countNews($catid);
        $this->load->library('pagination');
        $config['base_url'] = base_url().'news/cat/'.$catid.'/'.$this->uri->segment(4).'/';
		$config['total_rows'] = $nItem;
        $config['per_page'] = 2;
        $from = $this->uri->segment(5);
		$this->pagination->initialize($config);		
        $data['newsList'] = getNews($config['per_page'], $from, $catid);
        $data['newsCat'] = getCat();
        $data['view'] = 'v_news_cat';

        return $data;
    }
    
}