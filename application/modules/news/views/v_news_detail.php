<div role="main" class="main">
    <section class="page-header page-header-classic page-header-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                    <h1 data-title-border>Kabar Berita</h1>
                </div>
                <div class="col-md-4 order-1 order-md-2 align-self-center">
                    <ul class="breadcrumb d-block text-md-right">
                        <li><a href="#">Home</a></li>
                        <li class="active">News</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-4">
        <div class="row">
            <div class="col-lg-3 order-lg-2">
                <aside class="sidebar">
                    <form action="page-search-results.html" method="get">
                        <div class="input-group mb-3 pb-1">
                            <input class="form-control text-1" placeholder="Search..." name="s" id="s" type="text">
                            <span class="input-group-append">
                                <button type="submit" class="btn btn-dark text-1 p-2"><i
                                        class="fas fa-search m-2"></i></button>
                            </span>
                        </div>
                    </form>
                    <h5 class="font-weight-bold pt-4">Categories</h5>
                    <ul class="nav nav-list flex-column mb-5">
                        <?php foreach($newsCat as $cat) { ?>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'news/cat/'.$cat['id_cat'].'/'.str_replace(" ","-",strtolower($cat['cat_name']));?>"><?php echo $cat['cat_name'];?></a></li>							
                        <?php } ?>
                    </ul>	                
                    <h5 class="font-weight-bold pt-4">Tentang Sekarga</h5>
                    <?php echo getStaticContent('about_us_main_pages');?><br>
                    <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="<?php echo base_url();?>aboutus/tentang_sekarga" class="btn btn-xs btn-light text-1 text-uppercase">Read More</a></span>                               
                </aside>
            </div>
            <div class="col-lg-9 order-lg-1">
                <div class="blog-posts single-post">
                    <?php
                    foreach ($newsDetail as $news) {
                    ?>	
                    <article class="post post-large blog-single-post border-0 m-0 p-0">
                        <?php if ($news['video_link']) { ?>
							<div class="post-image">
								<div class="embed-responsive embed-responsive-16by9">									
									<iframe class="embed-responsive-item" src="<?php echo $news['video_link'];?>?showinfo=0&rel=0&enablejsapi=1&autoplay=0" width="640" height="360"></iframe>
								</div>
							</div>
                        <?php } ?>
                        <?php if ($news['images1']) { ?>
                        <div class="post-image">
                            <div class="owl-carousel owl-theme show-nav-hover dots-inside nav-inside nav-style-1 nav-light"
                                data-plugin-options="{'items': 1, 'margin': 10, 'loop': true, 'nav': true, 'dots': true}">
                                <div>
                                    <div class="img-thumbnail border-0 p-0 d-block">
                                        <img class="img-fluid border-radius-0" src="<?php echo base_url();?>assets/img/news/<?php echo $news['images1'];?>" alt="">
                                    </div>
                                </div>
                                <?php if ($news['images2']) { ?>
                                <div>
                                    <div class="img-thumbnail border-0 p-0 d-block">
                                        <img class="img-fluid border-radius-0" src="<?php echo base_url();?>assets/img/news/<?php echo $news['images2'];?>" alt="">
                                    </div>
                                </div>
                                <?php } if ($news['images3']) { ?>
                                <div>
                                    <div class="img-thumbnail border-0 p-0 d-block">
                                        <img class="img-fluid border-radius-0" src="<?php echo base_url();?>assets/img/news/<?php echo $news['images3'];?>" alt="">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="post-date ml-0">
                            <span class="day"><?php echo date('d', strtotime($news['publish_date']));?></span>
                            <span class="month"><?php echo date('M', strtotime($news['publish_date']));?></span>
                        </div>

                        <div class="post-content ml-0">
                            <h2 class="font-weight-bold"><?php echo $news['title'];?></h2>

                            <div class="post-meta">
                                <span><i class="far fa-user"></i> By <a href="#"><?php echo $news['author'];?></a> </span>
								<span><i class="far fa-folder"></i> <a href="<?php echo base_url().'news/cat/'.$news['id_cat'].'/'.str_replace(" ","-",strtolower($news['cat_name']));?>"><?php echo $news['cat_name'];?></a>                                
                            </div>

                            <?php echo $news['content_text'];?>

                            <div class="post-block mt-5 post-share">
                                <h4 class="mb-3">Share this Post</h4>

                                <!-- AddThis Button BEGIN -->
                                <div class="addthis_toolbox addthis_default_style ">
                                    <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                    <a class="addthis_button_tweet"></a>
                                    <a class="addthis_button_pinterest_pinit"></a>
                                    <a class="addthis_counter addthis_pill_style"></a>
                                </div>
                                <script type="text/javascript"
                                    src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
                                <!-- AddThis Button END -->

                            </div>
                        </div>
                    </article>
                    <?php } ?>

                </div>
            </div>
        </div>

    </div>

</div>