<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends MX_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('M_news');
    }

    function index() {        
        $data = $this->M_news->index();
        $this->load->view('main/v_main', $data);
    }

    function detail() {
        $id = $this->uri->segment(3);
        $data = $this->M_news->detail($id);
        $this->load->view('main/v_main', $data);
    }

    function cat() {
        $catid = $this->uri->segment(3);
        $data = $this->M_news->cat($catid);
        $this->load->view('main/v_main', $data);
    }

}