<div role="main" class="main">

    <!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
    
    <section class="page-header page-header-classic page-header-sm"> 
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>SEKRETARIAT SEKARGA</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="#">Home</a></li>
						<li class="active">Sekretariat Sekarga</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

    <div class="container">

        <div class="row py-4">           
            <div class="col-lg-6">

                <div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="800">
                    <h4 class="mt-2 mb-1"> <strong>Sekretariat</strong> Sekarga</h4>
                    <ul class="list list-icons list-icons-style-2 mt-2">
                        <li><i class="fas fa-map-marker-alt top-6"></i> <strong class="text-dark">Address:</strong> <?php echo getConfigItem('address');?></li>
                        <li><i class="fas fa-phone top-6"></i> <strong class="text-dark">Phone:</strong> <?php echo getConfigItem('phone');?></li>
                        <li><i class="fas fa-envelope top-6"></i> <strong class="text-dark">Email:</strong> <a href="mailto:<?php echo getConfigItem('email');?>"><?php echo getConfigItem('email');?></a></li>
                    </ul>
                </div>

                <div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="950">
                    <h4 class="pt-5">Working <strong>Hours</strong></h4>
                    <ul class="list list-icons list-dark mt-2">
                        <li><i class="far fa-clock top-6"></i> Monday - Friday - 9am to 3pm</li>
                        <li><i class="far fa-clock top-6"></i> Saturday & Sunday - Closed</li>                        
                    </ul>
                </div>                

            </div>

            <div class="col-lg-6">
                <div id="googlemaps" class="google-map mt-0" style="height: 500px;"></div>                
            </div>

        </div>

    </div>

</div>

<script src="<?php echo base_url();?>assets/js/views/view.contact.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY"></script>
<script>

    /*
    Map Settings

        Find the Latitude and Longitude of your address:
            - https://www.latlong.net/
            - http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

    */

    // Map Markers
    var mapMarkers = [{
        address: "Jl. Raya Duri Kosambi No.125 Jakbar 11750",
        html: "<strong>Employee House GITC</strong><br>Jl. Raya Duri Kosambi No.125 Jakbar 11750",
        icon: {
            image: "img/pin.png",
            iconsize: [26, 46],
            iconanchor: [12, 46]
        },
        popup: true
    }];

    // Map Initial Location
    var initLatitude = 40.75198;
    var initLongitude = -73.96978;

    // Map Extended Settings
    var mapSettings = {
        controls: {
            draggable: (($.browser.mobile) ? false : true),
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: true,
            streetViewControl: true,
            overviewMapControl: true
        },
        scrollwheel: false,
        markers: mapMarkers,
        latitude: initLatitude,
        longitude: initLongitude,
        zoom: 11
    };

    var map = $('#googlemaps').gMap(mapSettings);

    // Map text-center At
    var mapCenterAt = function(options, e) {
        e.preventDefault();
        $('#googlemaps').gMap("centerAt", options);
    }

</script>