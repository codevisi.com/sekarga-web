<div role="main" class="main">
	<section class="page-header page-header-classic page-header-sm"> 
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>DEWAN PIMPINAN PUSAT (DPP) SEKARGA</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="#">Home</a></li>
						<li class="active">Dewan Pimpinan Pusat</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="container pb-1">
		<div class="row pt-4">
			<div class="col">
				<div class="overflow-hidden mb-3">
					<h2 class="font-weight-bold text-center text-8 mb-0">
						<b>Struktur Organisasi Dewan Pimpinan Pusat (DPP) SEKARGA</b>													
					</h2>
				</div>
			</div>
		</div>

		<div class="row mb-2">
			<div class="col-lg-12">
				<div class="overflow-hidden">
					<p class="lead mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="250">
                        <?php 
                        $images = getStaticImages('about_us_dpp');
                        foreach ($images as $contentImages) {
                        ?>
                            <img alt="" class="img-fluid" src="<?php echo base_url();?>assets/img/news/<?php echo $contentImages['images1'];?>">
                        <?php    
                        }
                        ?>
					</p>
				</div>
			</div>			
		</div>		
	</div>	

	<div class="container">
		<div class="row text-center mt-5 mb-5">
			<div class="owl-carousel owl-theme carousel-center-active-item mb-0" data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 5}, '992': {'items': 3}, '1200': {'items': 3}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">
				<div>
					<a href="https://www.garuda-indonesia.com" target="_blank"><img class="img-fluid" src="<?php echo base_url();?>assets/img/logos/logo-1.png" alt="Garuda Indonesia" title="Garuda Indonesia"></a>
				</div>
				<div>
					<a href="https://www.itfglobal.org/" target="_blank"><img class="img-fluid" src="<?php echo base_url();?>assets/img/logos/logo-2.png" alt="International Transport Workers Federation" title="International Transport Workers Federation"></a>
				</div>
				<div>
					<a href="http://fspbumn.or.id/" target="_blank"><img class="img-fluid" src="<?php echo base_url();?>assets/img/logos/logo-3.png" alt="Federasi Serikat Pekerja BUMN" title="Federasi Serikat Pekerja BUMN"></a>
				</div>			
			</div>
		</div>
	</div>

</div>