<div role="main" class="main">
	<section class="page-header page-header-classic page-header-sm"> 
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>PROFIL SEKARGA</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="#">Home</a></li>
						<li class="active">Tentang Sekarga</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="container pb-1">
		<div class="row pt-4">
			<div class="col">
				<div class="overflow-hidden mb-3">
					<h2 class="word-rotator slide font-weight-bold text-8 mb-0 appear-animation" data-appear-animation="maskUp">
						<span class="word-rotator-words bg-primary">
							<b class="is-visible">Salam Solidaritas</b>
							<b>Bersatu Untuk Masa Depan</b>							
						</span>
					</h2>
				</div>
			</div>
		</div>

		<div class="row mb-2">
			<div class="col-lg-9">
				<div class="overflow-hidden">
					<p class="lead text-4 pt-2 font-weight-normal mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="250">
						<?php echo getStaticContent('about_us_header_section');?>
					</p>
				</div>
			</div>
			<div class="col-lg-3 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="450">
				<a href="<?php echo base_url();?>member" class="btn btn-rounded btn-3d btn-modern btn-secondary  btn-xl mb-2font-weight-bold mt-1 text-3">Gabung Bersama Kami <i class="fas fa-arrow-right ml-1"></i></a>
			</div>
			<div id="lambang-sekarga"></div>
		</div>		
	</div>	
	<section class="section section-height-1 bg-light border border-bottom-0 m-0" style="background-image: url(<?php echo base_url();?>assets/img/patterns/swirl_pattern.png); background-repeat: repeat;">
		<div class="container py-4">
			<div class="row align-items-center">
				<div class="col-md-6 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="1000">
					<div class="owl-carousel owl-theme nav-inside mb-0" data-plugin-options="{'items': 1, 'margin': 10, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 6000, 'loop': true}">
						<?php
						$staticImages = getStaticImages('about_us_lambang_sekarga');						
						foreach ($staticImages as $images) {
						?>
						<div>
							<img alt="" class="img-fluid" src="<?php echo base_url();?>assets/img/news/<?php echo $images['images1'];?>">
						</div>						
						<div>
							<img alt="" class="img-fluid" src="<?php echo base_url();?>assets/img/news/<?php echo $images['images2'];?>">
						</div>						
						<?php } ?>
					</div>
				</div>
				<div class="col-md-6">					
					<div class="heading heading-border heading-bottom-border">
						<h1 class="font-weight-normal appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500"><strong class="font-weight-extra-bold">Lambang</strong> Sekarga</h1> 
					</div>
					<p class="appear-animation lead text-4 pt-2 font-weight-normal" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="600"> <?php echo getStaticContent('about_us_lambang_sekarga');?></p>
				</div>
				<div id="tujuan-sekarga"></div>
			</div>
		</div>
	</section>
	
	<div class="container">		
		<div class="row mt-5 py-3">			
			<div class="col-md-12">
				<div class="overflow-hidden mb-5">
					<div class="heading heading-border heading-bottom-border">
						<h1 class="font-weight-normal text-center appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="500"><strong class="font-weight-extra-bold">Tujuan</strong> Sekarga</h1>
					</div>
				</div>				
			</div>
			<div class="col-md-6">
				<div class="toggle toggle-primary toggle-simple m-0" data-plugin-toggle>
					<section class="toggle active mt-0">
						<h4>Tujuan Serikat Karyawan Garuda Indonesia</h4>
						<div class="toggle-content">
							<?php echo getStaticContent('about_us_visi');?>	
						</div>
					</section>					
				</div>				
			</div>
			<div class="col-md-6 mt-10">
				<div class="toggle toggle-primary toggle-simple m-0" data-plugin-toggle>					
					<section class="toggle active mt-0">
						<h4>Dalam mencapai tujuan, Serikat Karyawan Garuda Indonesia melakukan kegiatan-kegiatan sbg</h4>
						<div class="toggle-content">
							<?php echo getStaticContent('about_us_misi');?>	
						</div>
					</section>
				</div>				
			</div>
		</div>
		<div class="row mt-3 pb-4">
			<div class="col text-center">
				<div class="heading heading-border heading-bottom-border">
					<h1 class="font-weight-normal appear-animation" data-appear-animation="fadeInDownShorter" data-appear-animation-delay="500"><strong class="font-weight-extra-bold">Kepengurusan</strong></h1> 
				</div>
				<p class="lead text-4 pt-2 font-weight-normal">
					Kepengurusan Serikat Karyawan PT. Garuda Indonesia (Persero) Tbk dibagi menjadi dua tingkat ;
				</p>
			</div>
		</div>
		<div class="row">								
			<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<ol class="list list-ordened list-ordened-style-3">
						<li><a href="<?php echo base_url()?>aboutus/dpp">Dewan Pimpinan Pusat (DPP)</a></li>
						<li><a href="<?php echo base_url()?>aboutus/dpc">Dewan Pimpinan Cabang (DPC)</a></li>
					</ol>
				</div>
			<div class="col-lg-4"></div>								
		</div>
		<div class="row">
			<div class="col py-4">
				<hr class="solid">
			</div>
		</div>
		<!-- <div class="row" id="sejarah-sekarga">
			<div class="col-md-8 mx-md-auto text-center">

				<h2 class="text-color-dark font-weight-normal text-5 mb-0 pt-2"><strong class="font-weight-extra-bold">Sejarah</strong> Sekarga</h2>
				<p Sejarah Sekarga dari awal mulai berdiri hingga saat ini.</p>

				<section class="timeline" id="timeline">
					<div class="timeline-body">
						<div class="timeline-date">
							<h3 class="text-primary font-weight-bold">1999</h3>
						</div>
						<article class="timeline-box left text-left appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="200">
							<div class="timeline-box-arrow"></div>
							<div class="p-2">
								<h3 class="font-weight-bold text-3 mt-3 mb-1">Berdiri</h3>
								<p class="mb-0 text-2">Serikat Karyawan PT. Garuda Indonesia (Persero) Tbk dibentuk berdasarkan deklarasi Karyawan PT. Garuda Indonesia (Persero) Tbk tanggal 18 Mei 1999 dengan batas waktu yang tidak ditentukan dan tercatat di Dinas Tenaga Kerja dan Transmigrasi Kotamadya Jakarta Pusat serta berlandaskan Pancasila dan UUD 1945.</p>
							</div>
						</article>

						<div class="timeline-date">
							<h3 class="text-primary font-weight-bold">2001</h3>
						</div>

						<article class="timeline-box right text-left appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="400">
							<div class="timeline-box-arrow"></div>
							<div class="p-2">
								<h3 class="font-weight-bold text-3 mt-3 mb-1">Kantor Sekretariat</h3>
								<p class="mb-0 text-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elementum, nulla vel pellentesque consequat.</p>
							</div>
						</article>

						<div class="timeline-date">
							<h3 class="text-primary font-weight-bold">2002</h3>
						</div>

						<article class="timeline-box left text-left appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="600">
							<div class="timeline-box-arrow"></div>
							<div class="p-2">
								<h3 class="font-weight-bold text-3 mt-3 mb-1">Pembentukan DPC pertama</h3>
								<p class="mb-0 text-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elementum, nulla vel consequat, ante.</p>
							</div>
						</article>
					</div>
				</section>

			</div>
		</div> -->

	</div>	

	<div class="container">
		<div class="row text-center mt-5 mb-5">
			<div class="owl-carousel owl-theme carousel-center-active-item mb-0" data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 5}, '992': {'items': 3}, '1200': {'items': 3}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">
				<div>
					<a href="https://www.garuda-indonesia.com" target="_blank"><img class="img-fluid" src="<?php echo base_url();?>assets/img/logos/logo-1.png" alt="Garuda Indonesia" title="Garuda Indonesia"></a>
				</div>
				<div>
					<a href="https://www.itfglobal.org/" target="_blank"><img class="img-fluid" src="<?php echo base_url();?>assets/img/logos/logo-2.png" alt="International Transport Workers Federation" title="International Transport Workers Federation"></a>
				</div>
				<div>
					<a href="http://fspbumn.or.id/" target="_blank"><img class="img-fluid" src="<?php echo base_url();?>assets/img/logos/logo-3.png" alt="Federasi Serikat Pekerja BUMN" title="Federasi Serikat Pekerja BUMN"></a>
				</div>			
			</div>
		</div>
	</div>

</div>