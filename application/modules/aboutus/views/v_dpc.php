<div role="main" class="main">

    <section class="page-header page-header-classic page-header-sm"> 
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>DEWAN PIMPINAN CABANG (DPC) SEKARGA</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="#">Home</a></li>
						<li class="active">Dewan Pimpinan Cabang</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

    <div class="container py-2">       

        <div class="row">
            <div class="col">
                <h4 class="mb-4">Dewan Pimpinan Cabang - Sekarga</h4>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="tabs tabs-vertical tabs-left tabs-navigation">
                            <ul class="nav nav-tabs col-sm-3">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#tabsNavigation1" data-toggle="tab"><i class="fas fa-users"></i> Struktur Organisasi DPC</a>
                                </li>
                                <?php foreach ($list_dpc as $dpc) { ?>                                
                                <li class="nav-item">
                                    <a class="nav-link" href="#<?php echo str_replace(" ","",strtolower($dpc['dpc_name']));?>" data-toggle="tab"><i class="fas fa-user"></i> <?php echo $dpc['dpc_name'];?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="tab-pane tab-pane-navigation active" id="tabsNavigation1">
                            <h4>Struktur Organisasi DPC</h4>
                            <p class="lead mb-0">
                                <span class='zoom' id='ex1'>
                                <?php 
                                $images = getStaticImages('about_us_dpc');
                                foreach ($images as $contentImages) {
                                ?>
                                    <img alt="" class="img-fluid" src="<?php echo base_url();?>assets/img/news/<?php echo $contentImages['images1'];?>">
                                <?php    
                                }
                                ?>
                                </span>
                            </p>
                        </div>
                        <?php foreach ($list_dpc as $dpc) { ?>                                
                        <div class="tab-pane tab-pane-navigation" id="<?php echo str_replace(" ","",strtolower($dpc['dpc_name']));?>">
                            <h4><?php echo $dpc['dpc_name'];?></h4>
                            <p class="lead mb-0">
                                <span class='zoom' id='ex1'>
                                <img alt="" class="img-fluid" src="<?php echo base_url();?>assets/img/dpc/<?php echo $dpc['images_org'];?>">
                                </span>
                            </p>                            
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>