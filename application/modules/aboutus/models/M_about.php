<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_about extends CI_Model {

	function __construct() {
		parent::__construct();
    }

    function tentang_sekarga() {        
        $data['view'] = 'v_tentang_sekarga';

        return $data;
    }

    function sekretariat() {
        $data['view'] = 'v_sekretariat';

        return $data;
    }

    function dpp() {        
        $data['view'] = 'v_dpp';

        return $data;
    }

    function dpc() {        
        $data['list_dpc'] = getDPC();
        $data['view'] = 'v_dpc';

        return $data;
    }
    
}