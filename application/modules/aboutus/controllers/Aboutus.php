<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aboutus extends MX_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('M_about');
    }

    function tentang_sekarga() {
        $data = $this->M_about->tentang_sekarga();
        $this->load->view('main/v_main', $data);
    }

    function sekretariat() {
        $data = $this->M_about->sekretariat();
        $this->load->view('main/v_main', $data);
    }

    function dpp() {
        $data = $this->M_about->dpp();
        $this->load->view('main/v_main', $data);
    }

    function dpc() {
        $data = $this->M_about->dpc();
        $this->load->view('main/v_main', $data);
    }

}
