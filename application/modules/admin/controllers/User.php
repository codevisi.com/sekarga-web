<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_admin');
        
        $isLogin = isLogin();        
        if (!$isLogin) redirect('admin/log/in');
        $isAdmin = isAdmin($this->session->userdata('usr_grp'));
        if (!$isAdmin) redirect('admin/log/in');
    }

    function index() {
        $data = $this->M_admin->user();
        $this->load->view('v_main', $data);
    }

    function group() {
        $data = $this->M_admin->user_grp();
        $this->load->view('v_main', $data);
    }

    function resetpass() {
        $username = $this->uri->segment(4);
        $passgen = passgen();
        changePass($username, $passgen);
        $detailUser = getDetailUser($username);      
        $messageAdd = "<hr><p>Username & Password anda : ".$username.' / '.$passgen."</p><p>Silahkan Login ke halaman member Sekarga  <a href='https://www.sekarga.or.id/member'>https://www.sekarga.or.id/member</a></p>";  

        $mailReg = sendmail($username, $detailUser[0]['nama'], 'Reset', $messageAdd);
        if ($mailReg) $this->session->set_flashdata('notif', 'Reset password berhasil, Email konfirmasi berhasil dikirim ke email '.$username);
        else $this->session->set_flashdata('notif', 'Reset password berhasil, Email konfirmasi gagal dikirim ke email '.$username.', cek alamat email');
        redirect('admin/user', 'refresh');
    }

    function setactive() {
        $username = $this->uri->segment(4);
        setActive($username);
        $passgen = passgen();
        changePass($username, $passgen);
        $detailUser = getDetailUser($username);      
        $messageAdd = "<hr><p>Username & Password anda : ".$username.' / '.$passgen."</p><p>Silahkan Login ke halaman member Sekarga  <a href='https://www.sekarga.or.id/member'>https://www.sekarga.or.id/member</a></p>";  

        $mailReg = sendmail($username, $detailUser[0]['nama'], 'Activation', $messageAdd);
		if ($mailReg) $this->session->set_flashdata('notif', 'Email konfirmasi aktivasi member berhasil dikirim ke email '.$username);
        else $this->session->set_flashdata('notif', 'Email konfirmasi aktivasi member gagal dikirim ke email '.$username.', cek alamat email');
        
        redirect('admin/user', 'refresh');
    }


}