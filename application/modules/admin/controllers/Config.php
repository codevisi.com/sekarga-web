<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_admin');
        
        $isLogin = isLogin();        
        if (!$isLogin) redirect('admin/log/in');
        $isAdmin = isAdmin($this->session->userdata('usr_grp'));
        if (!$isAdmin) redirect('admin/log/in');
    }

    function index() {
        $data = $this->M_admin->config();
        $this->load->view('v_main', $data);
    }

    function mailcontent() {
        $data = $this->M_admin->mailcontent();
        $this->load->view('v_main', $data);
    }

}