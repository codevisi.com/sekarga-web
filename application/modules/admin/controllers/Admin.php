<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('M_admin');
        
        $isLogin = isLogin();        
        if (!$isLogin) redirect('admin/log/in');
        $isAdmin = isAdmin($this->session->userdata('usr_grp'));
        if (!$isAdmin) redirect('admin/log/in');
    }

    function index() {
        $this->dashboard();
    }

    function dashboard() {
        $data = $this->M_admin->index();
        $this->load->view('v_main', $data);
    }

    function syncemp() {
        ini_set('max_execution_time', 0);
        $emp = getAllEmp();
        foreach($emp as $member) {
            $detailEmp = getEmpHR($member['nopeg']);
            // print_r($detailEmp);
            $data = array(
                'username' => $detailEmp['values'][0]['email'].'@garuda-indonesia.com',
                'nama' => $detailEmp['values'][0]['displayname'],
                'unit' => $detailEmp['values'][0]['department'],
                'position' => $detailEmp['values'][0]['jabatan'],
                'email' => $detailEmp['values'][0]['email'].'@garuda-indonesia.com'
            );

            // print_r($data);

            $where = array(
                'nopeg' => $member['nopeg']
            );

            update_data($where, 'tbl_user_copy', $data);
        }
    }
    
}