<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_admin');
        
        $isLogin = isLogin();        
        if (!$isLogin) redirect('admin/log/in');
        $isAdmin = isAdmin($this->session->userdata('usr_grp'));
        if (!$isAdmin) redirect('admin/log/in');
    }

    function header_images() {
        $data = $this->M_admin->header_images();
        $this->load->view('v_main', $data);
    }

    function static_content() {
        $data = $this->M_admin->static_content();
        $this->load->view('v_main', $data);
    }

    function news() {
        $data = $this->M_admin->news();
        $this->load->view('v_main', $data);
    }

    function category() {
        $data = $this->M_admin->content_cat();
        $this->load->view('v_main', $data);
    }

    function dpc() {
        $data = $this->M_admin->dpc();
        $this->load->view('v_main', $data);
    }

    function lapkeu() {
        $data = $this->M_admin->lapkeu();
        $this->load->view('v_main', $data);
    }

}