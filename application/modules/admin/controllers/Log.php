<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    function in() {       
        $isLogin = isLogin();                
        $isAdmin = isAdmin($this->session->userdata('usr_grp'));
        if ($isLogin && $isAdmin) redirect('admin');  
        
        if ($this->input->post()) {
            $post = $this->input->post();
            $auth = authenticate(trim($post['username']), trim($post['pwd']));            
            if ($auth && isAdmin) {
                setSession($auth);                
                redirect('admin');
            } else {
                $this->session->set_flashdata('msg', 'You not authorized to access...');
                $this->load->view('v_login');
            }
        } else 
            $this->load->view('v_login');
    }

    function out() {        
        $this->session->sess_destroy();
        header('location:'.base_url().'admin');
    }

    function lostpassword() {
        if ($this->input->post()) {
            $username = trim($this->input->post('username'));
            $passgen = passgen();
            changePass($username, $passgen);
            $detailUser = getDetailUser($username);
            if ($detailUser) {
                $messageAdd = "<hr><p>Password baru anda : ".$passgen."</p><p>Silahkan Login ke halaman admin Sekarga  <a href='https://www.sekarga.or.id/admin'>https://www.sekarga.or.id/admin</a></p>";  

                $mailReg = sendmail($username, $detailUser[0]['nama'], 'Lostadmin', $messageAdd);
                if ($mailReg) $this->session->set_flashdata('msg', 'Password berhasil dikirim ke email anda');
                else $this->session->set_flashdata('msg', 'Password gagal dikirim ke email anda');
            } else 
                $this->session->set_flashdata('msg', 'Email anda tidak terdaftar');

            $this->load->view('v_lostpass');
        } else 
            $this->load->view('v_lostpass');
    }

}