<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function index() {
		$data['view'] = 'v_dashboard';

		return $data;
	}
	
	function user() {
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->set_table('tbl_user')
			->set_subject('User / Member')
			->set_relation('usr_grp','tbl_user_grp', 'grp_name')
			->columns(array('username', 'nama', 'nopeg', 'unit', 'email', 'phone', 'usr_grp', 'isactive'))
			->fields('usr_grp', 'username', 'password', 'nama', 'nopeg', 'unit', 'position', 'email', 'phone', 'address', 'isactive', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->display_as('user_grp','User Group')
			->field_type('password', 'password')
			->field_type('isactive','dropdown', array('1' => 'Active', '2' => 'Inactive'))
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->order_by('nama', 'asc')
			->required_fields('username', 'nama', 'nopeg', 'unit', 'email', 'usr_grp')
			->add_action('Reset Password', '', 'admin/user/resetpass')
			->add_action('Activate Membership', '', 'admin/user/setactive')
			->callback_edit_field('password', function () {
                return '';
            })            			
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
		$output = $crud->render();
		$data['header_title'] = 'User / Member';
		$data['breadcrumb'] = 'List';
		$data['view'] = 'v_user';
        $data['output'] = (array)$output;

        return $data;
	}

	function add_action_log($post_array) {	
		$post_array['create_date'] = date('Y-m-d H:i:s');
		$post_array['create_by'] = $this->session->userdata('username');
		
		return $post_array;
	}	

	function edit_action_log($post_array) {			
		$post_array['mod_date'] = date('Y-m-d H:i:s');
		$post_array['mod_by'] = $this->session->userdata('username');
		
		return $post_array;
	}

	function user_grp() {
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->set_table('tbl_user_grp')
			->set_subject('User Group')
			->columns(array('grp_name', 'desc'))
			->display_as('grp_name','User Group Name')
			->display_as('desc','Notes')
			->fields('grp_name', 'desc', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->order_by('grp_name', 'asc')
			->required_fields('grp_name')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
		$output = $crud->render();
		$data['header_title'] = 'User Group';
		$data['breadcrumb'] = 'Group';
		$data['view'] = 'v_user';
        $data['output'] = (array)$output;

        return $data;
	}

	function config() {
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->set_table('tbl_config')
			->set_subject('Web Config')
			->columns(array('key', 'value'))
			->display_as('key','Config Name')
			->display_as('value','Value')
			->fields('key', 'value', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->order_by('key', 'asc')
			->required_fields('key', 'value')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
		$output = $crud->render();
		$data['header_title'] = 'Web Configuration';
		$data['breadcrumb'] = 'Configuration';
		$data['view'] = 'v_config';
        $data['output'] = (array)$output;

        return $data;
	}

	function menu_list() {
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->set_table('tbl_menu')
			->set_subject('User Menu')
			->set_relation('menu_parent','tbl_menu', 'menu_desc')
			->columns(array('menu_desc', 'menu_menu', 'menu_parent', 'menu_seq', 'is_active'))
			->display_as('menu_menu','URL')
			->display_as('menu_parent','Parent')
            ->display_as('menu_desc','Menu Name')
            ->display_as('menu_seq','Sort Order')
			->fields('menu_desc', 'menu_menu', 'menu_parent', 'menu_seq', 'is_active', 'subtitle', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->field_type('is_active','dropdown', array('0' => 'Inactive', '1' => 'Active'))
			->field_type('menu_id', 'hidden')
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->order_by('menu_seq', 'asc')
			->required_fields('menu_desc', 'menu_menu', 'menu_seq', 'is_active')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
		$output = $crud->render();
		$data['header_title'] = 'Menu List';
		$data['breadcrumb'] = 'list';
		$data['view'] = 'v_menu';
        $data['output'] = (array)$output;

        return $data;
	}

	function news() {
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->where('cat_name !=', 'Static Content');
		$crud->set_table('tbl_content')
			->set_subject('News')
			->set_relation('id_cat','tbl_content_cat', 'cat_name')
			->columns(array('id_cat', 'title', 'content_text', 'author', 'publish_date', 'is_active', 'create_date', 'create_by'))
			->display_as('id_cat','Category')
			->display_as('images_thumbnail','Images Thumbnail (400x300)')
			->display_as('images1','Images 1 (640x360)')
			->display_as('images2','Images 2 (640x360)')
			->display_as('images3','Images 3 (640x360)')
			->fields('id_cat', 'title', 'content_text', 'video_link', 'author', 'publish_date', 'tag', 'images_thumbnail', 'images1', 'images2', 'images3', 'is_active', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->field_type('is_active','dropdown', array('0' => 'Inactive', '1' => 'Active'))
			->set_field_upload('images_thumbnail','assets/img/news')
			->set_field_upload('images1','assets/img/news')
			->set_field_upload('images2','assets/img/news')
			->set_field_upload('images3','assets/img/news')
			->order_by('create_date', 'desc')
			->required_fields('id_cat', 'title', 'content_text', 'is_active')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
		$output = $crud->render();
		$data['header_title'] = 'News';
		$data['breadcrumb'] = 'News';
		$data['view'] = 'v_content';
        $data['output'] = (array)$output;

        return $data;
	}

	function static_content() {
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->where('cat_name', 'Static Content');
		$crud->set_table('tbl_content')
			->set_subject('Static Content')
			->set_relation('id_cat','tbl_content_cat', 'cat_name')
			->columns(array('id_cat', 'title', 'content_text', 'tag', 'is_active', 'create_date', 'create_by'))
			->display_as('id_cat','Category')
			->fields('id_cat', 'title', 'content_text', 'video_link', 'tag', 'images_thumbnail', 'images1', 'images2', 'images3', 'author', 'publish_date', 'is_active', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->field_type('is_active','dropdown', array('0' => 'Inactive', '1' => 'Active'))
			->set_field_upload('images_thumbnail','assets/img/news')
			->set_field_upload('images1','assets/img/news')
			->set_field_upload('images2','assets/img/news')
			->set_field_upload('images3','assets/img/news')
			->order_by('create_date', 'desc')
			->required_fields('id_cat', 'title', 'content_text', 'is_active')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
		$output = $crud->render();
		$data['header_title'] = 'Static Content';
		$data['breadcrumb'] = 'Static Content';
		$data['view'] = 'v_content';
        $data['output'] = (array)$output;

        return $data;
	}

	function content_cat() {
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->set_table('tbl_content_cat')
			->set_subject('Content Category')
			->columns(array('cat_name', 'desc'))
			->display_as('cat_name','Content Category')
			->display_as('desc','Desc')
			->fields('cat_name', 'desc', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->order_by('cat_name', 'asc')
			->required_fields('cat_name')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
		$output = $crud->render();
		$data['header_title'] = 'Content Category';
		$data['breadcrumb'] = 'Category';
		$data['view'] = 'v_content';
        $data['output'] = (array)$output;

        return $data;
	}

	function header_images() {
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->set_table('tbl_header_images')
			->set_subject('Header Images')
			->columns(array('title', 'images', 'text_top', 'text_middle', 'text_bottom', 'link', 'order', 'is_active'))
			->fields('title', 'images', 'text_top', 'text_middle', 'text_bottom', 'link', 'order', 'is_active', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->display_as('images','Images (1280x400)')
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->field_type('is_active','dropdown', array('0' => 'Inactive', '1' => 'Active'))
			->set_field_upload('images','assets/img/header')
			->order_by('order', 'asc')
			->required_fields('title', 'images', 'order', 'is_active')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
		$output = $crud->render();
		$data['header_title'] = 'Header Images';
		$data['breadcrumb'] = 'Header Images';
		$data['view'] = 'v_content';
        $data['output'] = (array)$output;

        return $data;
	}
	
	function mailcontent() {
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->set_table('tbl_mail_content')
			->set_subject('Mail Content')
			->columns(array('event', 'subject', 'message'))
			->fields('event', 'subject', 'message', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->order_by('event', 'asc')
			->required_fields('event', 'subject', 'message')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
		$output = $crud->render();
		$data['header_title'] = 'Web Settings';
		$data['breadcrumb'] = 'Mail Content';
		$data['view'] = 'v_content';
        $data['output'] = (array)$output;

        return $data;
	}
	
	function dpc() {
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->set_table('tbl_dpc')
			->set_subject('DPC')
			->columns(array('dpc_name', 'images_org'))
			->display_as('dpc_name','DPC')
			->display_as('images_org','Struktur Org')
			->fields('dpc_name', 'images_org', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->set_field_upload('images_org','assets/img/dpc')
			->order_by('dpc_name', 'asc')
			->required_fields('dpc_name')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
		$output = $crud->render();
		$data['header_title'] = 'Dewan Pimpinan Cabang';
		$data['breadcrumb'] = 'DPC';
		$data['view'] = 'v_content';
        $data['output'] = (array)$output;

        return $data;
	}

	function lapkeu() {
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->set_table('tbl_lapkeu')
			->set_subject('Laporan Keuangan')
			->columns(array('lap_name', 'doc'))
			->display_as('lap_name','Laporan')
			->display_as('doc','Document')
			->fields('lap_name', 'doc', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->set_field_upload('doc','assets/img/lapkeu')
			->order_by('lap_name', 'asc')
			->required_fields('lap_name')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
		$output = $crud->render();
		$data['header_title'] = 'Laporan Keuangan';
		$data['breadcrumb'] = 'DPP & DPC';
		$data['view'] = 'v_content';
        $data['output'] = (array)$output;

        return $data;
	}


}