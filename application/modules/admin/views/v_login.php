<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="Serikat Karyawan Garuda Indonesia, SEKARGA" />
		<meta name="description" content="Serikat Karyawan Garuda Indonesia - SEKARGA">
		<meta name="author" content="codevisi.com@gmail.com">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.png" type="image/x-icon" />

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/animate/animate.css">

		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/font-awesome/css/all.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url();?>assets/admin/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="<?php echo base_url();?>admin" class="logo float-left">
					<img src="<?php echo base_url();?>assets/admin/img/logo-sekarga.png" height="74" alt="Sekarga Web Admin" />
				</a>

				<div class="panel card-sign">
					<div class="card-title-sign mt-3 text-right">
						<h2 class="title text-uppercase font-weight-bold m-0"><i class="fas fa-user mr-1"></i> Administrator</h2>
					</div>
					<div class="card-body">
						<form action="<?php echo base_url();?>admin/log/in" method="post" id="formLogin">
							<?php if ($this->session->flashdata()) { ?>
							<div class="alert alert-primary">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<?php echo $this->session->flashdata('msg');?>
							</div>
							<?php } ?>
							<div class="form-group mb-3">
								<label>Username</label>
								<div class="input-group">
									<input name="username" type="text" class="form-control form-control-lg" required />
									<span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-3">
								<label class="float-left">Password</label>								
								<div class="input-group">
									<input name="pwd" type="password" class="form-control form-control-lg" required />
									<span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
										<input id="RememberMe" name="rememberme" type="checkbox"/>
										<label for="RememberMe">Remember Me</label>
									</div>
								</div>
								<div class="col-sm-4 text-right">
									<div class="clearfix">										
										<a href="<?php echo base_url();?>admin/log/lostpassword" class="float-right">Lupa Password?</a>
									</div>									
								</div>
							</div>

							<br>
							<div class="row">
								<div class="col-sm-8">
									<div class="g-recaptcha" data-sitekey="6LcoFqgZAAAAAA9G-mf4g9Dyh6omNzqYtbQUsQor"></div>
								</div>
								<div class="col-sm-4 text-right">
									<button type="submit" class="btn btn-primary mt-2">Sign In</button>
								</div>
							</div>							
						
						</form>
					</div>
				</div>

				<p class="text-center text-muted mt-3 mb-3">&copy; Copyright 2020. Serikat Karyawan Garuda Indonesia. All Rights Reserved.</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="<?php echo base_url();?>assets/admin/vendor/jquery/jquery.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/popper/umd/popper.min.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/common/common.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery.validation/jquery.validate.min.js"></script>
		<script src='https://www.google.com/recaptcha/api.js'></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url();?>assets/admin/js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url();?>assets/admin/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url();?>assets/admin/js/theme.init.js"></script>

		<script type="text/javascript">
		$("#formLogin").validate({
			submitHandler: function (form) {
				var response = grecaptcha.getResponse();
				//recaptcha failed validation
				if (response.length == 0) {
					$('#recaptcha-error').show();
					alert("Please click captcha !!")
					return false;
				}
				//recaptcha passed validation
				else {
					$('#recaptcha-error').hide();
					return true;
				}
			}
		});
		</script>

	</body>
</html>