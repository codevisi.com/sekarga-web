<?php foreach($output['css_files'] as $file):?>
<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>

<?php
foreach($output['js_files'] as $file):
    if (!strpos($file, 'jquery-1.11.1.min.js')) {
?>
<script src="<?php echo $file; ?>"></script>
<?php } 
endforeach; ?>

<section role="main" class="content-body">
    <header class="page-header">
        <h2><?php echo $header_title;?></h2>

        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="fas fa-home"></i>
                    </a>
                </li>
                <li><span>Home</span></li>
                <li><span>Web Settings</span></li>
                <li><span><?php echo $breadcrumb;?></span></li>
            </ol>

            <span class="sidebar-right-toggle">&nbsp;</span>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
    
                    <h2 class="card-title"><?php echo $header_title;?></h2>
                </header>
                <div class="card-body">
                <?php echo $output['output']; ?>
                </div>
            </section>
        </div>
    </div>
    <!-- end: page -->

</section>