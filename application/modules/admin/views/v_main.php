<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>SEKARGA - Serikat Karyawan Garuda Indonesia</title>	

		<meta name="keywords" content="Serikat Karyawan Garuda Indonesia, SEKARGA" />
		<meta name="description" content="Serikat Karyawan Garuda Indonesia - SEKARGA">
		<meta name="author" content="codevisi.com@gmail.com">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.png" type="image/x-icon" />

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/animate/animate.css">

		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/font-awesome/css/all.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/jquery-ui/jquery-ui.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/jquery-ui/jquery-ui.theme.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/bootstrap-multiselect/css/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/morris/morris.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/summernote/summernote-bs4.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url();?>assets/admin/vendor/jquery/jquery.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php echo $this->load->view('v_header');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php echo $this->load->view('v_sidebar');?>
				<!-- end: sidebar -->

				<!-- start content -->
				<?php echo $this->load->view($view);?>
				<!-- end content -->
			</div>

		</section>

	</body>

	<!-- Vendor -->	
	<script src="<?php echo base_url();?>assets/admin/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
	<script src="<?php echo base_url();?>assets/admin/vendor/popper/umd/popper.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/vendor/bootstrap/js/bootstrap.js"></script>
	<script src="<?php echo base_url();?>assets/admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url();?>assets/admin/vendor/common/common.js"></script>
	<script src="<?php echo base_url();?>assets/admin/vendor/nanoscroller/nanoscroller.js"></script>
	<script src="<?php echo base_url();?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.js"></script>
	<script src="<?php echo base_url();?>assets/admin/vendor/jquery-placeholder/jquery.placeholder.js"></script>
	
	<!-- Specific Page Vendor -->
	<script src="<?php echo base_url();?>assets/admin/vendor/jquery-ui/jquery-ui.js"></script>
	<script src="<?php echo base_url();?>assets/admin/vendor/jqueryui-touch-punch/jquery.ui.touch-punch.js"></script>
	<script src="<?php echo base_url();?>assets/admin/vendor/jquery-appear/jquery.appear.js"></script>
	<script src="<?php echo base_url();?>assets/admin/vendor/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
	<script src="<?php echo base_url();?>assets/admin/vendor/summernote/summernote-bs4.js"></script>
	
	<!-- Theme Base, Components and Settings -->
	<script src="<?php echo base_url();?>assets/admin/js/theme.js"></script>
	
	<!-- Theme Custom -->
	<script src="<?php echo base_url();?>assets/admin/js/custom.js"></script>
	
	<!-- Theme Initialization Files -->
	<script src="<?php echo base_url();?>assets/admin/js/theme.init.js"></script>

	<!-- Examples -->
	<script src="<?php echo base_url();?>assets/admin/js/examples/examples.dashboard.js"></script>
	<script src="<?php echo base_url();?>assets/admin/js/examples/examples.advanced.form.js"></script>
</html>