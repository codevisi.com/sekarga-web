<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="Serikat Karyawan Garuda Indonesia, SEKARGA" />
		<meta name="description" content="Serikat Karyawan Garuda Indonesia - SEKARGA">
		<meta name="author" content="codevisi.com@gmail.com">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/animate/animate.css">

		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/font-awesome/css/all.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url();?>assets/admin/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
                <a href="<?php echo base_url();?>admin" class="logo float-left">
					<img src="<?php echo base_url();?>assets/admin/img/logo-sekarga.png" height="74" alt="Sekarga Web Admin" />
				</a>

				<div class="panel card-sign">
					<div class="card-title-sign mt-3 text-right">
						<h2 class="title text-uppercase font-weight-bold m-0"><i class="fas fa-user mr-1"></i> Recover Password</h2>
					</div>
					<div class="card-body">
						<div class="alert alert-info">
							<p class="m-0">Masukkan email username anda, kami akan mengirimkan email konfirmasi akun anda</p>
						</div>

						<?php if ($this->session->flashdata()) { ?>
						<div class="alert alert-primary">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<?php echo $this->session->flashdata('msg')?>
						</div>
						<?php } ?>
						<form action="<?php echo base_url();?>admin/log/lostpassword" method="post">
							<div class="form-group mb-0">
								<div class="input-group">
									<input name="username" type="email" placeholder="E-mail" class="form-control form-control-lg" required />
									<span class="input-group-append">
                                        <input type="submit" value="Send" class="btn btn-primary btn-lg">
									</span>
								</div>
							</div>

							<p class="text-center mt-3"><a href="<?php echo base_url();?>admin/log/in">Kembali ke halaman login</a></p>
						</form>
					</div>
				</div>

				<p class="text-center text-muted mt-3 mb-3">&copy; Copyright 2020. Serikat Karyawan Garuda Indonesia. All Rights Reserved.</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="<?php echo base_url();?>assets/admin/vendor/jquery/jquery.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/popper/umd/popper.min.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/common/common.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="<?php echo base_url();?>assets/admin/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url();?>assets/admin/js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url();?>assets/admin/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url();?>assets/admin/js/theme.init.js"></script>

	</body>
</html>