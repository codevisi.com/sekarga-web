<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html"
            data-fire-event="sidebar-left-toggle">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">

                <ul class="nav nav-main">
                    <li class="<?php if ($this->uri->segment(2) == 'dashboard') echo 'nav-expanded nav-active';?>">
                        <a class="nav-link" href="<?php echo base_url();?>admin/dashboard">
                            <i class="fas fa-home" aria-hidden="true"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-parent <?php if ($this->uri->segment(2) == 'content') echo 'nav-expanded nav-active';?>">
                        <a class="nav-link" href="#">
                            <i class="fas fa-copy" aria-hidden="true"></i>
                            <span>Pages Content</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?php echo base_url();?>admin/content/header_images">
                                    Header Images
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="<?php echo base_url();?>admin/content/static_content">
                                    Static Content
                                </a>
                            </li>                    
                            <li>
                                <a class="nav-link" href="<?php echo base_url();?>admin/content/news">
                                    News
                                </a>
                            </li>                                                   
                            <li>
                                <a class="nav-link" href="<?php echo base_url();?>admin/content/category">
                                    Content Category
                                </a>
                            </li>                            
                            <li>
                                <a class="nav-link" href="<?php echo base_url();?>admin/content/dpc">
                                    List DPC
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="<?php echo base_url();?>admin/content/lapkeu">
                                    Laporan Keuangan
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent <?php if ($this->uri->segment(2) == 'menu') echo 'nav-expanded nav-active';?>">
                        <a class="nav-link" href="#">
                            <i class="fas fa-tasks" aria-hidden="true"></i>
                            <span>Menu Settings</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?php echo base_url();?>admin/menu">
                                    Menu List
                                </a>
                            </li>                                                 
                        </ul>
                    </li>
                    <li class="nav-parent <?php if ($this->uri->segment(2) == 'user') echo 'nav-expanded nav-active';?>">
                        <a class="nav-link" href="#">
                            <i class="fas fa-user" aria-hidden="true"></i>
                            <span>User / Member</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?php echo base_url();?>admin/user">
                                    User / Member List
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="<?php echo base_url();?>admin/user/group">
                                    User / Member Group
                                </a>
                            </li>                           
                        </ul>
                    </li>
                    <li class="nav-parent <?php if ($this->uri->segment(2) == 'config') echo 'nav-expanded nav-active';?>">
                        <a class="nav-link" href="#">
                            <i class="fas fa-cogs" aria-hidden="true"></i>
                            <span>Web Settings</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?php echo base_url()?>admin/config">
                                    Configuration
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="<?php echo base_url();?>admin/config/mailcontent">
                                    Mail Content Settings
                                </a>
                            </li>                            
                        </ul>
                    </li>  
                    <li>
                        <a class="nav-link" href="<?php echo base_url();?>" target="_blank">
                            <i class="fas fa-external-link-alt" aria-hidden="true"></i>
                            <span>Go To Front-End</em></span>
                        </a>                        
                    </li>                 
                </ul>
            </nav>                      
        </div>

        <script>
            // Maintain Scroll Position
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                    sidebarLeft.scrollTop = initialPosition;
                }
            }
        </script>


    </div>

</aside>
<!-- end: sidebar -->