<section role="main" class="content-body card-margin">
    <header class="page-header">
        <h2><?php echo $header_title;?></h2>

        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="fas fa-home"></i>
                    </a>
                </li>
                <li><span>Home</span></li>
                <li><span>Content</span></li>
                <li><span><?php echo $breadcrumb;?></span></li>
            </ol>

            <span class="sidebar-right-toggle">&nbsp;</span>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <form id="form" action="<?php echo base_url();?>admin/content/save_static_content" method="post" class="form-horizontal">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        </div>

                        <h2 class="card-title"><?php echo $breadcrumb;?></h2>
                        <p class="card-subtitle">
                            Click save to changes your content
                        </p>
                    </header>
                    <div class="card-body">                        
                        <div class="form-group row">
                            <label class="col-lg-2 control-label text-lg-right pt-2">Head Section</label>
                            <div class="col-lg-10">
                                <textarea name="content" class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'><?php echo getStaticContent('');?></textarea>
                            </div>
                            <input type="hidden" name="pages" value="<?php echo $breadcrumb;?>">
                        </div>
                    </div>
                    <footer class="card-footer">
                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn btn-default">Reset</button>
                            </div>
                        </div>
                    </footer>
                </section>
            </form>
        </div>       
    </div>    
    <!-- end: page -->
</section>