<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
    <div class="header-body">       
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-logo">
                            <a href="<?php echo base_url();?>">
                                <img alt="Sekarga" width="275" height="94" data-sticky-width="200" data-sticky-height="68" src="<?php echo base_url();?>assets/img/logo-sekarga.png">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="header-column justify-content-end">
                    <div class="header-row">
                        <div class="header-nav header-nav-line header-nav-top-line header-nav-top-line-with-border order-2 order-lg-1">
                            <div class="header-nav-main header-nav-main-square header-nav-main-font-lg header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-1 header-nav-main-sub-effect-1">
                                <nav class="collapse">
                                    <ul class="nav nav-pills" id="mainNav">
                                    <?php
                                    $userMenu = getMenu();
                                    if ($userMenu) {
                                        foreach($userMenu as $menu) {  
                                            if ($menu['menu_parent'] == 0) {        
                                    ?>
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle <?php if ($this->uri->segment(1) == strtolower($menu['menu_menu'])) echo ' active';?>" href="<?php echo base_url().$menu['menu_menu']?>">
                                                <?php echo $menu['menu_desc'];?>
                                            </a> 
                                            <?php
                                            //get submenu
                                            $subMenu = getSubMenu($menu['menu_id']);
                                            if ($subMenu) {      
                                            ?>
                                            <ul class="dropdown-menu">
                                                <?php                                                 
                                                    foreach($subMenu as $sMenu) {
                                                ?>
                                                    <li>
                                                        <a class="dropdown-item" href="<?php echo base_url().$sMenu['menu_menu']?>">
                                                            <?php echo $sMenu['menu_desc'];?>
                                                        </a>
                                                    </li>
                                                <?php
                                                    }                                                
                                                ?>                 
                                            </ul>
                                            <?php  } ?>                    
                                        </li>
                                    <?php                
                                            }
                                        }
                                    ?>
                                        <?php if($this->session->userdata('logged_in')) { ?>
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle <?php if ($this->uri->segment(1) == 'member') echo ' active';?>" href="<?php echo base_url().'member';?>">
                                                Member Area
                                            </a> 
                                        </li>
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle" href="<?php echo base_url().'member/logout';?>">
                                                Logout
                                            </a> 
                                        </li>
                                        <?php } else { ?>
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle <?php if ($this->uri->segment(1) == 'member') echo ' active';?>" href="<?php echo base_url().'member';?>">Masuk / Daftar</a> 
                                        </li>
                                        <?php } ?>
                                    <?php
                                    }
                                    ?>        
                                    </ul>
                                </nav>
                            </div>
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                                <i class="fas fa-bars"></i>
                            </button>                            
                            <div class="header-nav-features header-nav-features-no-border header-nav-features-lg-show-border">
                                <div class="header-nav-feature header-nav-features-search d-inline-flex">
                                    <a href="#" class="header-nav-features-toggle" data-focus="headerSearch"><i class="fas fa-search header-nav-top-icon"></i></a>
                                    <div class="header-nav-features-dropdown" id="headerTopSearchDropdown">
                                        <form role="search" action="page-search-results.html" method="get">
                                            <div class="simple-search input-group">
                                                <input class="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="Search...">
                                                <span class="input-group-append">
                                                    <button class="btn" type="submit">
                                                        <i class="fa fa-search header-nav-top-icon"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <ul class="header-social-icons social-icons d-none d-sm-block social-icons-clean">
                                <li class="social-icons-facebook"><a href="<?php echo getConfigItem('fb_pages');?>" target="_blank" title="Facebook Sekarga"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="social-icons-instagram"><a href="<?php echo getConfigItem('instagram_pages');?>" target="_blank" title="Instagram Sekarga"><i class="fab fa-instagram"></i></a></li>                                
                            </ul>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>        
    </div>
</header>
