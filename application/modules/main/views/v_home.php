<?php $this->load->view('v_main_slider');?>

<div class="home-intro bg-primary" id="home-intro">
	<div class="container">				
		<div class="row align-items-center">
			<div class="col-lg-7">
				<p>
					<span class="highlighted-word text-6">Salam Solidaritas, " Bersatu Untuk Masa Depan "</span> 
				</p>
			</div>
			<div class="col-lg-5">
				<div class="get-started text-left text-lg-right">
					<a href="<?php echo base_url();?>member" class="btn btn-rounded btn-3d btn-dark btn-xl text-4 font-weight-semibold px-3 py-3">Gabung Bersama Sekarga</a>
					<div class="text-4 text-color-dark mb-0 learn-more">atau <a href="<?php echo base_url();?>aboutus/tentang_sekarga" class="text-3 text-color-secondary">lihat profil kami</a></div>
				</div>
			</div>
		</div>				
	</div>
</div>

<div class="container">
	<div class="row text-center">
		<div class="col-md-10 mx-md-auto ">
			<h1 class="word-rotator slide font-weight-bold text-dark text-8 mb-2 appear-animation" data-appear-animation="fadeInUpShorter">
				<span>Sekilas </span>
				<span class="word-rotator-words bg-primary">
					<b class="is-visible">Tentang</b>
					<b>Profil</b>
				</span>
				<span> Serikat Karyawan Garuda Indonesia</span>
			</h1>
			<p class="text-color-dark text-5 line-height-5 mb-0 lead appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
				<?php echo getStaticContent('about_us_main_pages');?>
			</p>			
			<div class="row pb-5 mb-3 mt-3">
				<div class="col text-center">
					<a href="<?php echo base_url();?>aboutus/tentang_sekarga" class="btn btn-secondary btn-px-3 py-1 font-weight-semibold text-3 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">Selengkapnya <i class="fas fa-arrow-right ml-1"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>

<section class="section section-height-3 bg-light border border-bottom-0 m-0" style="background-image: url(<?php echo base_url();?>assets/img/patterns/swirl_pattern.png); background-repeat: repeat;">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="heading heading-border heading-bottom-border">
					<h1 class="font-weight-normal text-8 text-center pb-1">Tentang <strong class="font-weight-bold">SEKARGA</strong></h1>
				</div>
			</div>
		</div>
		<div class="row">			
			<div class="col-md-6 col-lg-4 mb-5 mb-lg-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">
				<div class="card flip-card flip-card-vertical text-center rounded-0">
					<div class="flip-front p-0">
						<div class="card card-background-image-hover border-0" style="background-image: url(<?php echo base_url();?>assets/img/blog/square/bgmiddle.jpg);">
						<div class="card-body text-center"><br><br><br><br>
							<i class="icon-organization icons text-color-primary text-10"></i>
							<h4 class="card-title mt-2 mb-2 text-5 font-weight-bold">Tujuan Organisasi</h4>
							<p class="card-text text-color-dark font-weight-normal">Tujuan Serikat Karyawan Garuda Indonesia <br><br></p>
						</div>
					</div>
					</div>
					<div class="flip-back d-flex align-items-center p-5" style="background-color: #FAAA17; background-size: cover; background-position: center;">
						<div class="flip-content my-4">
							<h4 class="font-weight-bold text-color-light">TUJUAN ORGANISASI</h4>
							<p class="font-weight-light text-color-light text-4">Tujuan berdirinya Serikat Karyawan Garuda Indonesia</p>
							<a href="<?php echo base_url();?>aboutus/tentang_sekarga#tujuan-sekarga" class="btn btn-light btn-modern text-2 text-color-dark font-weight-bold">SELENGKAPNYA</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 mb-5 mb-lg-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">
				<div class="card flip-card flip-card-vertical text-center rounded-0">
					<div class="flip-front p-0">
						<div class="card card-background-image-hover border-0" style="background-image: url(<?php echo base_url();?>assets/img/blog/square/bgmiddle.jpg);">
						<div class="card-body text-center"><br><br><br><br>
							<i class="icon-user icons text-color-primary text-10"></i>
							<h4 class="card-title mt-2 mb-2 text-5 font-weight-bold">Dewan Pimpinan Pusat <br></h4>
							<p class="card-text text-color-dark font-weight-normal">DPP - Serikat Karyawan Garuda Indonesia <br><br></p>
						</div>
					</div>
					</div>
					<div class="flip-back d-flex align-items-center p-5" style="background-color: #FAAA17; background-size: cover; background-position: center;">
						<div class="flip-content my-4">
						<h4 class="font-weight-bold text-color-light">DEWAN PIMPINAN PUSAT</h4>
							<p class="font-weight-light text-color-light text-4">Lebih lanjut tentang Dewan Pimpinan Pusat (DPP) Serikat Karyawan Garuda Indonesia</p>
							<a href="<?php echo base_url();?>aboutus/dpp" class="btn btn-light btn-modern text-2 text-color-dark font-weight-bold">SELENGKAPNYA</a>
						</div>
					</div>
				</div>
			</div>		
			<div class="col-md-6 col-lg-4 mb-5 mb-lg-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">
				<div class="card flip-card flip-card-vertical text-center rounded-0">
					<div class="flip-front p-0">
						<div class="card card-background-image-hover border-0" style="background-image: url(<?php echo base_url();?>assets/img/blog/square/bgmiddle.jpg);">
						<div class="card-body text-center"><br><br><br><br>
							<i class="icon-people icons text-color-primary text-10"></i>
							<h4 class="card-title mt-2 mb-2 text-5 font-weight-bold">Dewan Pimpinan Cabang <br></h4>
							<p class="card-text text-color-dark font-weight-normal">DPC - Serikat Karyawan Garuda Indonesia <br><br></p>
						</div>
					</div>
					</div>
					<div class="flip-back d-flex align-items-center p-5" style="background-color: #FAAA17; background-size: cover; background-position: center;">
						<div class="flip-content my-4">
						<h4 class="font-weight-bold text-color-light">DEWAN PIMPINAN CABANG</h4>
							<p class="font-weight-light text-color-light text-4">Lebih lanjut tentang Dewan Pimpinan CABANG (DPC) Serikat Karyawan Garuda Indonesia</p>
							<a href="<?php echo base_url();?>aboutus/dpc" class="btn btn-light btn-modern text-2 text-color-dark font-weight-bold">SELENGKAPNYA</a>
						</div>
					</div>
				</div>
			</div>	
		</div>

	</div>
</section>

<section class="section section-height-2 border-0 m-0">
	<div class="container py-4">
		<div class="row">			
			<div class="col-md-10 mx-md-auto ">
				<h1 class="word-rotator slide font-weight-bold text-center text-8 text-dark mb-5 appear-animation" data-appear-animation="fadeInUpShorter">
					<span class="word-rotator-words bg-primary">
						<b class="is-visible">Kabar Berita</b>
						<b>News</b>
						<b>Informasi</b>
						<b>Kegiatan</b>
					</span>
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="blog-posts">
					<div class="row">
						<?php foreach($newsList as $news) { ?>
						<div class="col-md-4 col-lg-3">
							<article class="post post-medium border-0 pb-0 mb-5">
								<div class="post-image">
									<a href="<?php echo base_url().'news/detail/'.$news['id_content'].'/'.str_replace(" ","-",strtolower($news['cat_name'])).'/'.str_replace(" ","-",strtolower($news['title']));?>">
										<img src="<?php echo base_url();?>assets/img/news/<?php echo $news['images_thumbnail'];?>" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="" />
									</a>
								</div>

								<div class="post-content">
									<h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2"><a href="<?php echo base_url().'news/detail/'.$news['id_content'].'/'.str_replace(" ","-",strtolower($news['cat_name'])).'/'.str_replace(" ","-",strtolower($news['title']));?>"><?php echo $news['title'];?></a></h2>
									<p><?php echo substr($news['content_text'],0,150) ;?></p>

									<div class="post-meta">
										<span><i class="far fa-user"></i> By <a href="#"><?php echo $news['author'];?></a> </span>
										<span><i class="far fa-folder"></i> <a href="<?php echo base_url().'news/cat/'.$news['id_cat'].'/'.str_replace(" ","-",strtolower($news['cat_name']));?>"><?php echo $news['cat_name'];?></a> </span>										
										<span class="d-block mt-2"><a href="<?php echo base_url().'news/detail/'.$news['id_content'].'/'.str_replace(" ","-",strtolower($news['cat_name'])).'/'.str_replace(" ","-",strtolower($news['title']));?>" class="btn btn-xs btn-light text-1 text-uppercase">Read More</a></span>
									</div>

								</div>
							</article>
						</div>								
						<?php } ?>				
					</div>				
				</div>
			</div>

		</div>

	</div>
</section>

<div class="container">
	<div class="row text-center mt-5 mb-5">
		<div class="owl-carousel owl-theme carousel-center-active-item mb-0" data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 5}, '992': {'items': 3}, '1200': {'items': 3}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">
			<div>
				<a href="https://www.garuda-indonesia.com" target="_blank"><img class="img-fluid" src="<?php echo base_url();?>assets/img/logos/logo-1.png" alt="Garuda Indonesia" title="Garuda Indonesia"></a>
			</div>
			<div>
				<a href="https://www.itfglobal.org/" target="_blank"><img class="img-fluid" src="<?php echo base_url();?>assets/img/logos/logo-2.png" alt="International Transport Workers Federation" title="International Transport Workers Federation"></a>
			</div>
			<div>
				<a href="http://fspbumn.or.id/" target="_blank"><img class="img-fluid" src="<?php echo base_url();?>assets/img/logos/logo-3.png" alt="Federasi Serikat Pekerja BUMN" title="Federasi Serikat Pekerja BUMN"></a>
			</div>			
		</div>
	</div>
</div>