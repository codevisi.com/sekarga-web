<div role="main" class="main">

    <section class="page-header page-header-classic page-header-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                    <h1 data-title-border>KABAR BERITA</h1>
                </div>
                <div class="col-md-4 order-1 order-md-2 align-self-center">
                    <ul class="breadcrumb d-block text-md-right">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="active">Page Not Found</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <section class="http-error">
            <div class="row justify-content-center py-3">
                <div class="col-md-12 text-center">
                    <div class="http-error-main">
                        <h2>404!</h2>
                        <p>We're sorry, but the page you were looking for doesn't exist.</p>
                    </div>
                </div>							
            </div>
        </section>

    </div>

</div>
