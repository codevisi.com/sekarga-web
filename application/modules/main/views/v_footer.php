<footer id="footer">
	<div class="container">
		<div class="footer-ribbon">
			<span class="text-7">BERSATU UNTUK MASA DEPAN</span>
		</div>
		<div class="row py-5 my-4">
			<div class="col-md-6 col-lg-4 mb-2 mb-lg-0">
				<a href="index.html" class="logo pr-0 pr-lg-3">
					<img alt="Sekarga Website" src="<?php echo base_url();?>assets/img/logo-footer.png" class="opacity-7 bottom-4" height="80">
				</a>
				<p class="mt-2 mb-0 mr-5 text-4"><?php echo getStaticContent('footer_notes');?></p>				
			</div>
			<div class="col-md-6 col-lg-1 mb-2 mb-md-0">&nbsp;</div>
			<div class="col-md-6 col-lg-4 mb-2 mb-md-0">
				<div class="contact-details">
					<h5 class="text-5 text-transform-none font-weight-semibold text-color-light mb-4">Contact Details</h5>
					<ul class="list list-icons list-icons-lg text-4">
						<li class="mb-1"><i class="far fa-dot-circle text-color-primary"></i><p class="m-0"><a href="<?php echo base_url()?>aboutus/sekretariat"><?php echo getConfigItem('address');?></a></p></li>
						<li class="mb-1"><i class="fas fa-phone text-color-primary"></i><p class="m-0"><a href="tel:<?php echo getConfigItem('phone');?>"><?php echo getConfigItem('phone');?></a></p></li>
						<li class="mb-1"><i class="far fa-envelope text-color-primary"></i><p class="m-0"> <a href="mailto:<?php echo getConfigItem('email');?>"><?php echo getConfigItem('email');?></a></p></li>
					</ul>
				</div>
			</div>			
			<div class="col-md-6 col-lg-2">
			<h5 class="text-5 text-transform-none font-weight-semibold text-color-light mb-4">Follow Us</h5>
				<ul class="social-icons">
					<li class="social-icons-facebook"><a href="<?php echo getConfigItem('fb_pages');?>" target="_blank" title="Sekarga Facebook Pages"><i class="fab fa-facebook-f"></i></a></li>
					<li class="social-icons-instagram"><a href="<?php echo getConfigItem('instagram_pages');?>" target="_blank" title="Sekarga Instagram Pages"><i class="fab fa-instagram"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="py-2">
			<div class="row py-4">
				<div class="col d-flex align-items-center justify-content-center mb-4 mb-lg-0 text-4">
					<p>© Copyright 2020 - <?php echo date('Y')?>. Serikat Karyawan Garuda Indonesia - All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>

