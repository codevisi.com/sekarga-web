<div role="main" class="main">
    <section class="page-header page-header-classic page-header-sm"> 
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>KIRIM PESAN</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="#">Home</a></li>
						<li class="active">Hubungi Kami</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

    <div class="container">

        <div class="row py-4">
            <div class="col-lg-6">
                <?php if ($this->session->flashdata('msg')) { ?>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <i class="fas fa-exclamation-triangle"></i><?php echo $this->session->flashdata('msg');?>                                        
                </div>
                <?php } ?>
                <div class="overflow-hidden mb-1">
                    <h2 class="font-weight-normal text-7 mt-2 mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200"><strong class="font-weight-extra-bold">Hubungi</strong> Kami</h2>
                </div>
                <div class="overflow-hidden mb-4 pb-3">
                    <p class="mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="400">Silahkan masukkan pesan anda dan kirim ke kami</p>
                </div>

                <form id="formRegister" class="contact-form" action="<?php echo base_url();?>main/contactus" method="POST">
                    <div class="contact-form-success alert alert-success d-none mt-4" id="contactSuccess">
                        <strong>Success!</strong> Your message has been sent to us.
                    </div>
                
                    <div class="contact-form-error alert alert-danger d-none mt-4" id="contactError">
                        <strong>Error!</strong> There was an error sending your message.
                        <span class="mail-error-message text-1 d-block" id="mailErrorMessage"></span>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-lg-6">
                            <label class="required font-weight-bold text-dark text-2">Nama Anda</label>
                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="required font-weight-bold text-dark text-2">Alamat Email</label>
                            <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold text-dark text-2">Subject</label>
                            <input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" id="subject" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="required font-weight-bold text-dark text-2">Pesan</label>
                            <textarea maxlength="5000" data-msg-required="Please enter your message." rows="8" class="form-control" name="message" id="message" required></textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <div class="g-recaptcha" data-sitekey="6LcoFqgZAAAAAA9G-mf4g9Dyh6omNzqYtbQUsQor"></div>
                        </div>
                        <div class="form-group col text-4">
                            <input type="submit" value="Kirim Pesan" class="btn btn-primary btn-modern float-right" data-loading-text="Loading...">
                        </div>
                    </div>                    
                </form>

            </div>
            <div class="col-lg-6">

                <div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="800">
                    <h4 class="mt-2 mb-1">Sekretariat <strong>Sekarga</strong></h4>
                    <ul class="list list-icons list-icons-style-2 mt-2">
                        <li><i class="fas fa-map-marker-alt top-6"></i> <strong class="text-dark">Alamat:</strong> <br><?php echo getConfigItem('address');?></li>
                        <li><i class="fas fa-phone top-6"></i> <strong class="text-dark">Phone:</strong> <?php echo getConfigItem('phone');?></li>
                        <li><i class="fas fa-envelope top-6"></i> <strong class="text-dark">Email:</strong> <br> <a href="mailto:sekarga@garuda-indonesia.com">sekarga@garuda-indonesia.com</a>  <br> 
                        <a href="mailto:<?php echo getConfigItem('email');?>"><?php echo getConfigItem('email');?></a></li>
                    </ul>
                </div>

                <div id="googlemaps" class="google-map mt-0" style="height: 500px;"></div>

            </div>

        </div>

    </div>    

</div>

<div class="container">
	<div class="row text-center mt-5 mb-5">
		<div class="owl-carousel owl-theme carousel-center-active-item mb-0" data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 5}, '992': {'items': 3}, '1200': {'items': 3}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">
			<div>
				<a href="https://www.garuda-indonesia.com" target="_blank"><img class="img-fluid" src="<?php echo base_url();?>assets/img/logos/logo-1.png" alt="Garuda Indonesia" title="Garuda Indonesia"></a>
			</div>
			<div>
				<a href="https://www.itfglobal.org/" target="_blank"><img class="img-fluid" src="<?php echo base_url();?>assets/img/logos/logo-2.png" alt="International Transport Workers Federation" title="International Transport Workers Federation"></a>
			</div>
			<div>
				<a href="http://fspbumn.or.id/" target="_blank"><img class="img-fluid" src="<?php echo base_url();?>assets/img/logos/logo-3.png" alt="Federasi Serikat Pekerja BUMN" title="Federasi Serikat Pekerja BUMN"></a>
			</div>			
		</div>
	</div>
</div>