<!DOCTYPE html>
<html>
	<head>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-H1LF10QF2Y"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-H1LF10QF2Y');
		</script>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>SEKARGA - Serikat Karyawan Garuda Indonesia</title>	

		<meta name="keywords" content="Serikat Karyawan Garuda Indonesia, SEKARGA" />
		<meta name="description" content="Serikat Karyawan Garuda Indonesia - SEKARGA">
		<meta name="author" content="codevisi.com@gmail.com">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.png" type="image/x-icon" />

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/theme-elements.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/theme-blog.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/theme-shop.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/zoom.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/rs-plugin/css/navigation.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/circle-flip-slideshow/css/component.css">
		
		<!-- Demo CSS -->


		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/skins/default.css"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url();?>assets/vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body>
		<div class="body">
			<!-- Header Section  -->
			<?php echo $this->load->view('v_header');?>
			
			<div role="main" class="main">				
				<!-- Main sages -->
				<?php echo $this->load->view($view);?>
			</div>

			<!-- Footer section -->
			<?php echo $this->load->view('v_footer');?>
		</div>

		<!-- Vendor -->
		<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery.cookie/jquery.cookie.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/popper/umd/popper.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/common/common.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery.validation/jquery.validate.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/vide/jquery.vide.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/vivus/vivus.min.js"></script>
		<script src='https://www.google.com/recaptcha/api.js'></script>

		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url();?>assets/js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="<?php echo base_url();?>assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/views/view.home.js"></script>
		<script src="<?php echo base_url();?>assets/js/zoom.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url();?>assets/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url();?>assets/js/theme.init.js"></script>

		<script type="text/javascript">
		$("#formRegister").validate({
			submitHandler: function (form) {
				var response = grecaptcha.getResponse();
				//recaptcha failed validation
				if (response.length == 0) {
					$('#recaptcha-error').show();
					alert("Please click captcha !!")
					return false;
				}
				//recaptcha passed validation
				else {
					$('#recaptcha-error').hide();
					return true;
				}
			}
		});

		$("#loginForm").validate({			
			rules: {
				newpass1: {
					minlength : 8,
				},
				newpass2: {
					minlength : 8,
					equalTo: "#newpass1"
				}
			},
			messages: {
				newpass1: " Enter Password Minimal 8 Character",
				newpass2: " Enter Confirm Password Same as Password"
			}			
		});

		$(document).ready(function(){
			$('#file').on('change', function() {
				var file_data = $('#file').prop('files')[0];
        		var form_data = new FormData();	
				form_data.append('file', file_data);
	
				$.ajax({
						url         : '<?php echo base_url();?>member/upload_profil_photo',    
						dataType    : 'text',
						cache       : false,
						contentType : false,
						processData : false,
						data        : form_data,                         
						type        : 'post',
						success     : function(output){
							alert(output);
							window.location.replace("<?php echo base_url();?>member/<?php echo $this->uri->segment(2);?>");             
						}
				});
				$('#file').val('');  

			});

			$('#ex1').zoom();
			$('#ex2').zoom({ on:'grab' });
			$('#ex3').zoom({ on:'click' });			 
			$('#ex4').zoom({ on:'toggle' });	
		});

		</script>

		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjiAnxKyOXrjGGf-u2Qo3HJbD8GYhDf78"></script>
		<script>

			/*
			Map Settings

				Find the Latitude and Longitude of your address:
					- http://universimmedia.pagesperso-orange.fr/geo/loc.htm
					- http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

			*/

			// Map Markers
			var mapMarkers = [{
				address: "<?php echo getConfigItem('address');?>",
				html: "<?php echo getConfigItem('address');?>",
				icon: {
					image: "<?php echo base_url();?>assets/img/pin.png",
					iconsize: [26, 46],
					iconanchor: [12, 46]
				},
				popup: true
			}];

			// Map Initial Location
			var initLatitude = -6.1754611;
			var initLongitude = 106.7202469;

			// Map Extended Settings
			var mapSettings = {
				controls: {
					panControl: true,
					zoomControl: true,
					mapTypeControl: true,
					scaleControl: true,
					streetViewControl: true,
					overviewMapControl: true
				},
				scrollwheel: false,
				markers: mapMarkers,
				latitude: initLatitude,
				longitude: initLongitude,
				zoom: 16
			};

			var map = $('#googlemaps').gMap(mapSettings);

			// Map Center At
			var mapCenterAt = function(options, e) {
				e.preventDefault();
				$('#googlemaps').gMap("centerAt", options);
			}

		</script>
		
	</body>
</html>
