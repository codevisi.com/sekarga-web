<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_main extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function index() {
		$data['newsList'] = getNews(8);		
		$data['view'] = 'v_home';
		$data['slide'] = getSlideImages();

		return $data;
	}

	function page404() {
		$data['view'] = 'v_404';

		return $data;
	}
	
	function contactus() {
		$data['view'] = 'v_contactus';

		return $data;
    }
    
}