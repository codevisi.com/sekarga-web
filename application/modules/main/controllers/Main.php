<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MX_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('M_main');
    }

    function index() {
        $data = $this->M_main->index();
        $this->load->view('v_main', $data);
    }
    
    function page404() {
        $data = $this->M_main->page404();
        $this->load->view('v_main', $data);
    }

    function contactus() {
        if ($this->input->post()) {
            $post = $this->input->post();
            $mailTo = getConfigItem('email');
            $mailContact = sendContact($mailTo, $post['email'], $post['name'], $post['subject'], $post['message']);
            if ($mailContact) $this->session->set_flashdata('msg', 'Pesan anda berhasil dikirim ke SEKARGA');
            else $this->session->set_flashdata('msg', 'Pesan anda gagal dikirim ke SEKARGA');

            redirect('contactus');
        } else {
            $data = $this->M_main->contactus();
            $this->load->view('v_main', $data);
        }        
    }
}