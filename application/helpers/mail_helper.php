<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function sendmail($emailto, $nama, $event, $param=null) {
    $CI=& get_instance();

    $stmp_account = getConfigItem('smtp_email');
    $stmp_pass = getConfigItem('smtp_pass');
    $mailContent = getMailContent($event);
    $message = 'Dear '.ucwords(strtolower($nama));
    if ($event == 'Registration') {
        $message .= '<p>Pendaftaran anda sebagai anggota Sekarga Berhasil, berikut data yang anda masukkan, </p>'; 
        $message .= '<p>'; 
        $message .= 'Nama : '.$param["nama"].'<br>';
        $message .= 'Nopeg : '.$param["nopeg"].'<br>';
        $message .= 'Unit : '.$param["unit"].'<br>';
        $message .= 'Telp : '.$param["nohp"].'<br>';
        $message .= 'Email : '.$param["email"].'<br>';
        $message .= '</p>'; 
    }

    $message .= $mailContent[0]['message'];
    if ($param) {
        if ($event <> 'Registration') $message .= $param;
    }
    
    $config = [
        'mailtype'  => 'html',
        'charset'   => 'utf-8',
        'protocol'  => 'smtp',
        'smtp_host' => 'mail.sekarga.or.id',
        'smtp_user' => $stmp_account,
        'smtp_pass'   => $stmp_pass,
        'smtp_crypto' => 'ssl',
        'smtp_port'   => 465,
        'crlf'    => "\r\n",
        'newline' => "\r\n"
    ];

    $CI->load->library('email', $config);
    $CI->email->from($stmp_account, 'SEKARGA');
    $CI->email->to($emailto);
    $cc = array('sekargaofficial@gmail.com', 'sekarga.garudaindonesia@gmail.com');
    $CI->email->cc($cc);
    $CI->email->subject($mailContent[0]['subject']);
    $CI->email->message($message);

    if ($CI->email->send()) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function sendContact($to, $from, $nama, $subject, $message) {
    $CI=& get_instance();
    $CI->load->library('email');
    
    $stmp_account = getConfigItem('smtp_email');
    $stmp_pass = getConfigItem('smtp_pass');
    
    // Mail config
    $mailSubject = 'Pesan dikirim dari '.$nama;
    
    // Mail content
    $mailContent = '
        <h2>Pesan Dari Website Sekarga</h2>
        <p><b>Name: </b>'.$nama.'</p>
        <p><b>Email: </b>'.$from.'</p>
        <p><b>Subject: </b>'.$subject.'</p>
        <p><b>Message: </b>'.$message.'</p>
    ';

    $config = [
        'mailtype'  => 'html',
        'charset'   => 'utf-8',
        'protocol'  => 'smtp',
        'smtp_host' => 'smtp.gmail.com',
        'smtp_user' => $stmp_account,
        'smtp_pass'   => $stmp_pass,
        'smtp_crypto' => 'ssl',
        'smtp_port'   => 465,
        'crlf'    => "\r\n",
        'newline' => "\r\n"
    ];
        
    $CI->email->initialize($config);
    $CI->email->from($from, $nama);
    $CI->email->to($to);
    $cc = array('sekargaofficial@gmail.com', 'sekarga.garudaindonesia@gmail.com');
    $CI->email->cc($cc);
    $CI->email->subject($mailSubject);
    $CI->email->message($mailContent);
    
    // Send email & return status
    if ($CI->email->send()) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function getMailContent($event) {
    $CI=& get_instance();
    $CI->load->database('default');
    $CI->db->select('subject, message')->from('tbl_mail_content');
    $CI->db->where('event', $event);
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->result_array();
    else return FALSE;
}