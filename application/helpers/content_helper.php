<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function getConfigItem($id) {
    $CI=& get_instance();
    $CI->load->database('default');
    $CI->db->select('value')->from('tbl_config');
    $CI->db->where('key', $id);
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->row()->value;
    else return FALSE;
}

function getStaticContent($pages) {
    $CI=& get_instance();
    $CI->load->database('default');
    $CI->db->select('content_text')->from('tbl_content');
    $CI->db->where('title', $pages);
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->row()->content_text;
    else return FALSE;
}

function getStaticImages($pages) {
    $CI=& get_instance();
    $CI->load->database('default');
    $CI->db->select('images1, images2, images3')->from('tbl_content');
    $CI->db->where('title', $pages);
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->result_array();
    else return FALSE;
}

function getNews($limit=null, $offset=null, $catid=null) {
    $CI=& get_instance();
    $CI->load->database('default');
    $CI->db->select('tbl_content.*, tbl_content_cat.cat_name')->from('tbl_content');
    $CI->db->join('tbl_content_cat', 'tbl_content.id_cat = tbl_content_cat.id_cat');
    if ($catid) $CI->db->where('tbl_content.id_cat', $catid);
    else $CI->db->where('tbl_content.id_cat <>', 105);
    $CI->db->where('is_active', 1);
    if ($limit) $CI->db->limit($limit, $offset);
    $CI->db->order_by('publish_date', 'desc');
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->result_array();
    else return FALSE;
}

function getCat($catid=null) {
    $CI=& get_instance();
    $CI->load->database('default');
    $CI->db->select('*')->from('tbl_content_cat');
    if ($catid) $CI->db->where('id_cat', $catid);
    else $CI->db->where('id_cat <>', 105);
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->result_array();
    else return FALSE;
}

function getNewsDetail($id) {
    $CI=& get_instance();
    $CI->load->database('default');
    $CI->db->select('tbl_content.*, tbl_content_cat.cat_name')->from('tbl_content');
    $CI->db->join('tbl_content_cat', 'tbl_content.id_cat = tbl_content_cat.id_cat');
    $CI->db->where('id_content', $id);
    $CI->db->where('is_active', 1);
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->result_array();
    else return FALSE;
}

function countNews($catid = null) {
    $CI=& get_instance();
    $CI->load->database('default');
    $CI->db->select('tbl_content.*, tbl_content_cat.cat_name')->from('tbl_content');
    $CI->db->join('tbl_content_cat', 'tbl_content.id_cat = tbl_content_cat.id_cat');
    if ($catid) $CI->db->where('tbl_content.id_cat', $catid);
    else $CI->db->where('tbl_content.id_cat <>', 105);
    $CI->db->where('is_active', 1);
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->num_rows();
    else return FALSE;
}

function getSlideImages() {
    $CI=& get_instance();
    $CI->load->database('default');
    $CI->db->select('*')->from('tbl_header_images');
    $CI->db->where('is_active', 1);
    $CI->db->order_by('order');
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->result_array();
    else return FALSE;
}

function getDPC() {
    $CI=& get_instance();
    $CI->load->database('default');
    $CI->db->select('*')->from('tbl_dpc');
    $CI->db->order_by('dpc_name');
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->result_array();
    else return FALSE;
}

function getLapKeu() {
    $CI=& get_instance();
    $CI->load->database('default');
    $CI->db->select('*')->from('tbl_lapkeu');
    $CI->db->order_by('lap_name');
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->result_array();
    else return FALSE;
}