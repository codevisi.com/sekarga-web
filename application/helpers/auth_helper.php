<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function isLogin() {
    $CI=& get_instance();
    if ($CI->session->userdata('logged_in')) return TRUE;
    else return FALSE;
}

function authenticate($user, $pass) {
    $CI=& get_instance();
    $CI->load->database('default');
    $CI->db->select('username, nama, unit, nopeg, position, email, usr_grp')->from('tbl_user');
    $CI->db->where('username', $user);
    $CI->db->where('password', md5('sek'.$pass.'arga'));
    $CI->db->where('isactive', 1);
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->result_array();
    else return FALSE;
}

function isAdmin($usrGrp) {
    $CI=& get_instance();    
    if ($usrGrp == 101) return TRUE;
    else return FALSE;
}

function setSession($usr) {
    $CI=& get_instance();
    $sess_data['logged_in'] = TRUE;
    $sess_data['username']= $usr[0]['username'];
    $sess_data['nama']= $usr[0]['nama'];
    $sess_data['unit']= $usr[0]['unit'];
    $sess_data['nopeg']= $usr[0]['nopeg'];
    $sess_data['email']= $usr[0]['email'];
    $sess_data['usr_grp']= $usr[0]['usr_grp'];

    $CI->session->set_userdata($sess_data);
}

function getUserGrpName($usrGrp) {    
    $CI=& get_instance();
    $CI->load->database('default');
    $CI->db->select('grp_name')->from('tbl_user_grp');
    $CI->db->where('id_grp', $usrGrp);    
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->row()->grp_name;
    else return FALSE;
}

function changePass($id, $pass) {
    $CI =& get_instance();
    $CI->load->database('default');
    $CI->db->set('password', md5('sek'.$pass.'arga'));
    $CI->db->where('username', $id);
    $CI->db->update('tbl_user'); 
    $CI->db->close();
}

function setActive($id) {
    $CI =& get_instance();
    $CI->load->database('default');
    $CI->db->set('isactive', 1);
    $CI->db->where('username', $id);
    $CI->db->update('tbl_user'); 
    $CI->db->close();
}

function getMenu() {
    $CI =& get_instance();
    $CI->load->database('default');
    $CI->db->select("*");
    $CI->db->from("tbl_menu tum");
    $CI->db->where("tum.is_active", 1);
    $CI->db->order_by("tum.menu_seq");

    $q = $CI->db->get(); 
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->result_array();
    else return FALSE;
}

function getSubMenu($parent, $grp=null) {	
    $CI =& get_instance();
    $CI->load->database('default');
    $CI->db->select('*')->from('tbl_menu tum');
    $CI->db->where('tum.menu_parent', $parent);	
    $CI->db->where('tum.is_active', 1);	
    $CI->db->order_by('tum.menu_seq');
	
    $q = $CI->db->get();
    $CI->db->close();
	if ($q->num_rows() > 0) return $q->result_array();
	else return FALSE;
}

function isMember($email, $id = null) {
    $CI =& get_instance();
    $CI->load->database('default');
    $CI->db->select('isactive')->from('tbl_user');
    if ($id) $CI->db->where('nopeg', $id);	
    $CI->db->or_where('username', $email);	
    
    $q = $CI->db->get();
    $CI->db->close();
    if ($q->num_rows() > 0) return $q->row()->isactive;        
	else return FALSE;
}

function isEmail($email) {
    $CI =& get_instance();
    $CI->load->database('default');
    $CI->db->select('username')->from('tbl_user');
    $CI->db->where('username', $email);	
    
    $q = $CI->db->get();
    $CI->db->close();
    if ($q->num_rows() > 0) return $q->row()->isactive;        
	else return FALSE;
}

function regNewUser($userData) {
    $CI =& get_instance();
    $CI->load->database('default');
    $data = array(
        'username' => $userData['email'],
        'nama' => strtoupper($userData['nama']),
        'nopeg' => $userData['nopeg'],
        'unit' => strtoupper($userData['unit']),        
        'email' => $userData['email'],
        'phone' => $userData['nohp'],
        'usr_grp' => 102,
        'isactive' => 2
    );

    $CI->db->insert('tbl_user', $data);
}

function passgen()  {
    $data = '1234567890ABDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
    return substr(str_shuffle($data), 0, 8);
}

function getDetailUser($id) {
    $CI =& get_instance();
    $CI->load->database('default'); 
    $CI->db->select('*')->from('tbl_user');
    $CI->db->where('username', $id);	
    
    $q = $CI->db->get();
    $CI->db->close();
    if ($q->num_rows() > 0) return $q->result_array();        
	else return FALSE;
}

function getPhoto($id) {
    $CI =& get_instance();
    $CI->load->database('default'); 
    $CI->db->select('photo')->from('tbl_user');
    $CI->db->where('username', $id);	
    
    $q = $CI->db->get();
    $CI->db->close();
    if ($q->num_rows() > 0) return $q->row()->photo;        
	else return FALSE;
}

function update_data($where, $table, $data) {
    $CI =& get_instance();
    $CI->load->database('default'); 
    $CI->db->where($where);
    $CI->db->update($table,$data);
    $CI->db->close();
}

function checkOldPass($id, $pass) {
    $CI =& get_instance();
    $CI->load->database('default'); 
    $CI->db->select('password')->from('tbl_user');
    $CI->db->where('username', $id);	
    $CI->db->where('password', md5('sek'.$pass.'arga'));	
    
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return TRUE;        
	else return FALSE;
}

function deletePhoto($id) {
    $photo = getPhoto($id);
    if ($photo) {
        $path_to_file = './assets/img/member/'.$photo;
        unlink($path_to_file);     
    }
}

function getAllEmp() {
    $CI =& get_instance();
    $CI->load->database('default');
    $CI->db->select('*')->from('tbl_user_copy');
    // $CI->db->limit(5);
 
    $q = $CI->db->get();
    $CI->db->close();
    if ($q->num_rows() > 0) return $q->result_array();   
	else return FALSE;
}